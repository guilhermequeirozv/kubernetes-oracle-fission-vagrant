### 1) Get vagrant files for Ubuntu.

> [Vagrant files](https://github.com/justmeandopensource/vagrant)

### 2) Installation of Kubernetes.

> [Install Kubernetes Cluster using kubeadm](https://github.com/justmeandopensource/kubernetes/blob/master/docs/install-cluster-ubuntu-20.md)
