###  1) Get Oracle 19.

> [Download Oracle 19c](https://download.oracle.com/otn/linux/oracle19c/190000/LINUX.X64_193000_db_home.zip)
```

cp LINUX.X64_193000_db_home.zip oracle/19.3.0/
```

###  2) Zip fmisir folder and copy it to appropriated folder.

```
zip -r fmisir.zip fmisir/

cp fmisir.zip oracle/19.3.0/
```

###  3) Install Oracle.

```
cd oracle
./buildDockerImage.sh -v 19.3.0 -e
```

###  4) Start Oracle Docker.

Create a shared directory for moving files:
```
sudo mkdir oracleSharedDir/
```

Change owner to Oracle Uid (default value):
```
sudo chown 54321:54321 -R oracleSharedDir/
docker image ls | grep oracle
```

Create and starts a docker container:
```
docker run -d --name 19.3.0-ee -p 1521:1521 -p 5500:5500 -v /home/cohen/oracleSharedDir:/home/oracle oracle/database:19.3.0-ee
```

Wait for the completion of Oracle Database (also takes a lot of time):
```
docker logs -f 19.3.0-ee
```

```
...
## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 
DATABASE IS READY TO USE!
## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 
```

###  5) Management of Oracle Docker.

Stops container:
```
docker stop -t 120 19.3.0-ee
```

Start container:
```
docker start 19.3.0-ee
```

Enters into container:
```
docker exec -it 19.3.0-ee bash
```

###  6) Configure Oracle user.

Type the following commands in docker terminal:
```
cat >> .bashrc

ORACLE_SID=ORCLCDB
ORACLE_PDB=ORCLPDB1
ORACLE_PDB_SID=ORCLPDB1
ORAENV_ASK=NO
source oraenv
```
(finishes by pressing ctlr + D)

Relog into docker bash (exit and relogin).

 
###  7) After configuring ORACLE PDB and user. Install fmisir library with a user with priviledges to create libraries, procedures, operators, types and indextypes.

```
export ORAUSER=myorauser
export ORAPWD=myoraped
cd fmisir/install
./install.sh
```
