
#include <string>

#include "logger.h"

#ifndef _LIBSLIM_H
#include "libslim.h"
#endif

/* Auxiliar functions */

string toString(double n) {
    stringstream strout;
    strout << n;
    return strout.str();
} //end toString

char * toUpper(char *str) {
    char *ret = new char[strlen(str) + 1];
    for (int i = 0; i < strlen(str); i++)
        ret[i] = toupper(str[i]);
    ret[strlen(str)] = '\0';
    return ret;
}

size_t raiseException(OCIExtProcContext *ctx, text *errbuf) {
    int errnum = 29400; /* Oracle error number */
    /* ORA-29400:    data cartridge error string
       Cause:    An error has occurred in a data cartridge external procedure. This message will be followed by a second message giving more details about the data cartridge error.
       Action:    See the data cartridge documentation for an explanation of the second error message.
     */

    return OCIExtProcRaiseExcpWithMsg(ctx, errnum, errbuf, strlen((char *) errbuf));
}

bool checkerr(OCIExtProcContext *ctx, OCIError *errhp, sword status) {
    text errbuf[512];
    sb4 errcode = 0;

    switch (status) {
        case OCI_SUCCESS:
            return false;
        case OCI_ERROR:
            (void) OCIErrorGet((dvoid *) errhp, (ub4) 1, (text *) NULL, &errcode,
                    errbuf, (ub4)sizeof (errbuf), OCI_HTYPE_ERROR);
            raiseException(ctx, errbuf);
            return true;
        default:
            logger.message("Unknown error code: " + toString(status));
            raiseException(ctx, (text *) "SLM-0000: Unknown error");
    }
    return true;
}

/*
 * Write a LOB to a LobLocator
 */
int writeLOB(
        OCIExtProcContext *ctx,
        OCISvcCtx *svchp,
        OCIError *errhp,
        OCILobLocator * lob,
        unsigned char * buffer,
        unsigned int bufferLength) {

    oraub8 byteAmt, charAmt;
    oraub8 offset = 1;

    /* Open the LOB */
    if (checkerr(ctx, errhp, OCILobOpen(svchp, errhp, lob, OCI_LOB_READWRITE))) /* OCI Programmer's Guide p. 17-74 */
        return -1;

    byteAmt = charAmt = (oraub8) bufferLength; // byte_amt and char_amt are IN/OUT parameters

    /* Writing to LOB in one-piece mode */
    if (checkerr(ctx, errhp, OCILobWrite2(svchp, errhp, lob, &byteAmt, &charAmt,
            offset, (void *) buffer, (oraub8) bufferLength,
            OCI_ONE_PIECE, (void *) 0,
            (OCICallbackLobWrite2) 0,
            0, SQLCS_IMPLICIT))) {
        /* Closing the LOB is mandatory if you have opened it */
        OCILobClose(svchp, errhp, lob);
        return -1;
    }

    /* Closing the LOB is mandatory if you have opened it */
    if (checkerr(ctx, errhp, OCILobClose(svchp, errhp, lob)))
        return -1;

    return 1;
} //end writeLOB

/*
 * Read the LOB pointed by the LobLocator
 */
int readLOB(
        OCIExtProcContext *ctx,
        OCISvcCtx *svchp,
        OCIError *errhp,
        OCILobLocator * lob,
        unsigned char ** buffer,
        unsigned int * bufferLength) {

    oraub8 byteAmt, charAmt;
    oraub8 offset = 1;

    oraub8 lobLength;

    /* Open the LOB */
    if (checkerr(ctx, errhp, OCILobOpen(svchp, errhp, lob, OCI_LOB_READONLY))) /* OCI Programmer's Guide p. 17-74 */
        return -1;

    /* get the LOB length */
    if (checkerr(ctx, errhp, OCILobGetLength2(svchp, errhp, lob, &lobLength))) {
        /* Closing the LOB is mandatory if you have opened it */
        OCILobClose(svchp, errhp, lob);
        return -1;
    }

    if (lobLength == 0) { /* Empty element; not readable */
        /* Closing the LOB is mandatory if you have opened it */
        if (checkerr(ctx, errhp, OCILobClose(svchp, errhp, lob)))
            return -1;
        return 0;
    }

    *bufferLength = (int) lobLength;
    *buffer = new unsigned char[lobLength];

    /* read the LOB content */
    byteAmt = charAmt = lobLength;
    if (checkerr(ctx, errhp, OCILobRead2(svchp, errhp, lob, &byteAmt, &charAmt, offset,
            (void *) * buffer, lobLength, OCI_ONE_PIECE, (void *) 0,
            (OCICallbackLobRead2) 0,
            (ub2) 0, (ub1) SQLCS_IMPLICIT))) {
        /* Closing the LOB is mandatory if you have opened it */
        OCILobClose(svchp, errhp, lob);
        return -1;
    }

    /* Closing the LOB is mandatory if you have opened it */
    if (checkerr(ctx, errhp, OCILobClose(svchp, errhp, lob)))
        return -1;

    return 1;
} //end readLOB


/**
 * Load a signature from a space delimited feature file.
 * @param numFeatures have to be less than or equals than the number of features in the file.
 * @param offset is the number of features to be ignored before starting to load the features.
 * E.g. if offset = 2, the first feature to be read is the 3rd. This is useful to ignore, for example, the object name and/or class at the beginning of the signature.
 *
 * create or replace FUNCTION  readSignatureFromFile (absoluteFileName VARCHAR2, signature IN OUT NOCOPY BLOB, numFeatures PLS_INTEGER, offset PLS_INTEGER)
 * RETURN PLS_INTEGER
 * AS EXTERNAL LIBRARY libslim NAME "readSignatureFromFile"
 * WITH CONTEXT;
 */
int readSignatureFromFile(
        OCIExtProcContext * ctx,
        char * absoluteFileName,
        OCILobLocator **signature,
        int numFeatures,
        int offset) {

    OCIEnv *envhp = (OCIEnv *) 0; /* env. handle */
    OCISvcCtx *svchp = (OCISvcCtx *) 0; /* service handle */
    OCIError *errhp = (OCIError *) 0; /* error handle */

    if (numFeatures < 1) {
        string msg = "SLM-0001: The number of features must be greater than or equals to 1. Number of features: " + toString(numFeatures);
        raiseException(ctx, (text *) msg.c_str());
        return OCI_ERROR;
    }

    ifstream in(absoluteFileName);
    if (!in.is_open()) {
        string msg = "SLM-0002: I/O error when opening feature file: " + string(absoluteFileName);
        raiseException(ctx, (text *) msg.c_str());
        return OCI_ERROR;
    }

    float features[numFeatures];
    int i = 0;

    while ((i < offset) && in.good()) {
        in.ignore(200, ' '); //ignore the feature
        i++;
    }

    while ((i < (numFeatures + offset)) && in.good()) {
        in >> features[i - offset];
        i++;
    }

    if (in.fail() || i != (numFeatures + offset)) {
        string msg = "SLM-0003: Feature file corrupted: " + string(absoluteFileName);
        raiseException(ctx, (text *) msg.c_str());
        return OCI_ERROR;
    }

    in.close();

    stRowId rowId; //memory area reserved for the Oracle RowId attribute
    myBasicArrayObject * obj = new myBasicArrayObject(numFeatures, features, ROWID_SIZE, rowId, 0);

    /* Get the environment */
    if (checkerr(ctx, errhp, OCIExtProcGetEnv(ctx, &envhp, &svchp, &errhp)))
        return OCI_ERROR;

    //    if (SetObjToLOB(ctx, svchp, errhp, *signature, obj) != 1) {
    if (writeLOB(ctx, svchp, errhp, *signature, (unsigned char *) obj->Serialize(), obj->GetSerializedSize()) != 1) {
        raiseException(ctx, (text *) "SLM-0004: Error writing features to LOB");
        return OCI_ERROR;
    }

    delete obj;
    obj = 0;

    logger.message(string(absoluteFileName) + " OK");

    return OCI_SUCCESS;
} //end readSignatureFromFile

int checkSignature(
        OCIExtProcContext *ctx,
        OCILobLocator *signature,
        int numFeatures) {
    
    OCIEnv *envhp = (OCIEnv *) 0; /* env. handle */
    OCISvcCtx *svchp = (OCISvcCtx *) 0; /* service handle */
    OCIError *errhp = (OCIError *) 0; /* error handle */

    unsigned char * buffer = 0;
    unsigned int bufferLength = 0;

    myBasicArrayObject * obj;

    const int EMPTY_SIGNATURE = 0;
    const int SIGNATURE_OK = 1;
    const int WRONG_NUMBER_OF_FEATURES = -1; /* it may indicate that the signature is corrupted or is not comparable to the desired signature pattern */

    /* Get the environment */
    if (checkerr(ctx, errhp, OCIExtProcGetEnv(ctx, &envhp, &svchp, &errhp)))
        return OCI_ERROR;

    /* Read the LOBs stored in the signatures and calculate the distance */
    switch (readLOB(ctx, svchp, errhp, signature, &buffer, &bufferLength)) {

        case 0: // occurs when invoking EMPTY_BLOB() function
            return EMPTY_SIGNATURE;

        case 1:
        {
            /* Get the object stored in the signature */
            obj = new myBasicArrayObject();
            obj->Unserialize(buffer, bufferLength);
            delete[] buffer;
            buffer = 0;
            /* Compare the signature's number of features to the provided number of features */
            if (obj->GetSize() != numFeatures) {
                delete obj;
                obj = 0;
                return WRONG_NUMBER_OF_FEATURES;
            }

            /* Output the signature */
            //char outStr[5000];
            //char dst[10];
            //strcpy(outStr, "");
            for (int k = 0; k < numFeatures; k++) {
                logger.message(to_string(*((float *) obj->Get(k))));
                //sprintf(dst, " %5.0f", *((float *) obj->Get(k)));
                //strcat(outStr, dst);
            }
            //logger.message(outStr);

            delete obj;
            obj = 0;
            return SIGNATURE_OK;
        }

    }

    /* If the function did not returned yet, an error occurred and an exception was raised */
    return OCI_ERROR;
} //end checkSignature

float genericDistance(
        OCIExtProcContext *ctx,
        OCILobLocator *signature1,
        OCILobLocator *signature2,
        myBasicMetricEvaluator *evaluator) {

    OCIEnv *envhp = (OCIEnv *) 0; /* env. handle */
    OCISvcCtx *svchp = (OCISvcCtx *) 0; /* service handle */
    OCIError *errhp = (OCIError *) 0; /* error handle */

    unsigned char * buffer;
    unsigned int bufferLength;

    myBasicArrayObject *obj1, *obj2;
    stDistance distance = -1; //-1 Indicates an error

    /* Get the environment */
    if (checkerr(ctx, errhp, OCIExtProcGetEnv(ctx, &envhp, &svchp, &errhp)))
        return (float) distance;


    /* Read the LOBs stored in the signatures and calculate the distance */
    switch (readLOB(ctx, svchp, errhp, signature1, &buffer, &bufferLength)) {

        case 0: // occurs when invoking EMPTY_BLOB() function
            raiseException(ctx, (text *) "SLM-0011: Empty signature in the first argument");
            break;

        case 1:
        {
            /* Get the object stored in the first signature */
            obj1 = new myBasicArrayObject();
            obj1->Unserialize(buffer, bufferLength);
            delete[] buffer;
            buffer = 0;

            switch (readLOB(ctx, svchp, errhp, signature2, &buffer, &bufferLength)) {
                case 0: // occurs when invoking EMPTY_BLOB() function
                    raiseException(ctx, (text *) "SLM-0011: Empty signature in the second argument");
                    break;

                case 1:
                {
                    /* Get the object stored in the first signature */
                    obj2 = new myBasicArrayObject();
                    obj2->Unserialize(buffer, bufferLength);
                    delete[] buffer;
                    buffer = 0;

                    if (obj1->GetSize() != obj2->GetSize())
                        raiseException(ctx, (text *) "SLM-0013: The signatures provided are not comparable");
                    else {
                        distance = evaluator->GetDistance(obj1, obj2);
                    }
                    delete obj2;
                    obj2 = 0;
                    break;
                }

                default:
                    raiseException(ctx, (text *) "SLM-0012: Error unserializing signature in the second argument");
            }
            delete obj1;
            obj1 = 0;
            break;
        }

        default:
            raiseException(ctx, (text *) "SLM-0012: Error unserializing signature in the first argument");

    } //end switch

    return (float) distance;
} //end genericDistance

float euclideanDistance(
        OCIExtProcContext *ctx,
        OCILobLocator *signature1,
        OCILobLocator *signature2) {

    float ret;
    myBasicEuclideanMetricEvaluator *evaluator = new myBasicEuclideanMetricEvaluator();
    ret = genericDistance(ctx, signature1, signature2, evaluator);
    delete evaluator;
    return ret;
} //end euclideanDistance

float manhattanDistance(
        OCIExtProcContext *ctx,
        OCILobLocator *signature1,
        OCILobLocator *signature2) {

    float ret;
    myBasicManhattanMetricEvaluator *evaluator = new myBasicManhattanMetricEvaluator();
    ret = genericDistance(ctx, signature1, signature2, evaluator);
    delete evaluator;
    return ret;
} //end manhattanDistance

float canberraDistance(
        OCIExtProcContext *ctx,
        OCILobLocator *signature1,
        OCILobLocator *signature2) {

    float ret;
    myBasicCanberraMetricEvaluator *evaluator = new myBasicCanberraMetricEvaluator();
    ret = genericDistance(ctx, signature1, signature2, evaluator);
    delete evaluator;
    return ret;
} //end canberraDistance

/* Oracle extensible indexing code for SlimTree */

OCINumber * slimGenericIndexCreate(
        OCIExtProcContext *ctx,
        ODCIIndexInfo *ix,
        ODCIIndexInfo_ind *ix_ind,
        char *parms,
        myBasicMetricEvaluator *evaluator) {

    OCIEnv *envhp = (OCIEnv *) 0; /* env. handle */
    OCISvcCtx *svchp = (OCISvcCtx *) 0; /* service handle */
    OCIError *errhp = (OCIError *) 0; /* error handle */

    int retval = (int) ODCI_SUCCESS; /* return from this function */
    OCINumber *rval = (OCINumber *) 0;

    ODCIColInfo *colinfo = (ODCIColInfo *) 0; /* stores the column information of the ix->IndexCols */
    ODCIColInfo_ind *colinfo_ind = (ODCIColInfo_ind *) 0; /* colinfo indicator */
    boolean exists = TRUE; /* used in the function to get the ix->IndexCols */

    OCIStmt *stmthp = (OCIStmt *) 0; /* statement handle */
    OCIDefine *defnp1 = (OCIDefine *) 0; /* define for the ROWID attribute of the SELECT list */
    OCIDefine *defnp2 = (OCIDefine *) 0; /* define for the BLOB attribute of the SELECT list */
    sword status; /* stores the status of the fetchs */

    stDiskPageManager *pmSlim;
    mySlimTree *slim;

    OCILobLocator *signature;
    myBasicArrayObject *obj;
    stRowId rowId;

    unsigned char * buffer = 0;
    unsigned int bufferLength = 0;

    /* Create the index file */
    string fileName = DIRNAME + string((char *) OCIStringPtr(envhp, ix->IndexSchema)) + "." + string((char *) OCIStringPtr(envhp, ix->IndexName));
    stSize pageSize = (stSize) atoi(parms); // If no valid conversion could be performed, a zero value is returned
    if (pageSize < 64) {
        raiseException(ctx, (text *) "SLM-0021: Wrong page size for the indextype");
        return rval;
    }

    /* Get the environment */
    if (checkerr(ctx, errhp, OCIExtProcGetEnv(ctx, &envhp, &svchp, &errhp)))
        return rval;

    /* Get the table and the attribute whose index is being created */
    if (checkerr(ctx, errhp, OCICollGetElem(envhp, errhp,
            (OCIColl *) ix->IndexCols, (sb4) 0, &exists,
            (void **) & colinfo, (void **) & colinfo_ind)))
        return rval;

    /* Compose the statement */
    string query = "SELECT ROWID, " + string((char *) OCIStringPtr(envhp, colinfo->ColName)) +
            " FROM " + string((char *) OCIStringPtr(envhp, colinfo->TableName));

    /* Allocate stmt handle */
    if (checkerr(ctx, errhp,
            OCIHandleAlloc((dvoid *) envhp, (dvoid **) &(stmthp),
            (ub4) OCI_HTYPE_STMT, (size_t) 0,
            (dvoid **) 0)))
        return rval;

    /* Prepare the statement */
    if (checkerr(ctx, errhp,
            OCIStmtPrepare(stmthp, errhp, (text *) query.c_str(),
            (ub4) strlen(query.c_str()), OCI_NTV_SYNTAX,
            OCI_DEFAULT)))
        return rval;

    /* Allocate a LobLocator descriptor to hold the BLOB results */
    if (checkerr(ctx, errhp, OCIDescriptorAlloc((void *) envhp, (void **) & signature,
            (ub4) OCI_DTYPE_LOB, (size_t) 0, (void **) 0)))
        return rval;

    /* Set up define for the ROWID results */
    if (checkerr(ctx, errhp, OCIDefineByPos(stmthp, &(defnp1), errhp,
            (ub4) 1, (dvoid *) rowId,
            (sb4) ROWID_SIZE,
            (ub2) SQLT_CHR, (dvoid *) 0, (ub2 *) 0,
            (ub2 *) 0, (ub4) OCI_DEFAULT)))
        return (rval);

    /* Set up define for the BLOB results */
    if (checkerr(ctx, errhp, OCIDefineByPos(stmthp, &(defnp2), errhp,
            (ub4) 2, (dvoid *) &(signature),
            (sb4) - 1,
            (ub2) SQLT_BLOB, (dvoid *) 0, (ub2 *) 0,
            (ub2 *) 0, (ub4) OCI_DEFAULT)))
        return (rval);

    /* Create the index file */
    try {
        pmSlim = new stDiskPageManager(fileName, pageSize);
    } catch (io_error &e) {
        raiseException(ctx, (text *) "SLM-0022: I/O error when creating index file");
        return rval;
    }

    /* Create the index */
    slim = new mySlimTree(pmSlim, evaluator);

    /* Adjust the MAM parameters */
    slim->SetChooseMethod(mySlimTree::cmMINDIST);
    slim->SetSplitMethod(mySlimTree::smMINMAX);

    /* Execute the query */
    if (checkerr(ctx, errhp, OCIStmtExecute(svchp, stmthp, errhp, (ub4) 0,
            (ub4) 0, (OCISnapshot *) NULL,
            (OCISnapshot *) NULL,
            (ub4) OCI_DEFAULT)))
        return (rval);

    /* Fetch the first row */
    status = OCIStmtFetch2(stmthp, errhp, (ub4) 1, (ub2) OCI_FETCH_NEXT,
            (sb4) 0, (ub4) OCI_DEFAULT);

    obj = new myBasicArrayObject();

    while ((status == OCI_SUCCESS) || (status == OCI_SUCCESS_WITH_INFO)) {

        switch (readLOB(ctx, svchp, errhp, signature, &buffer, &bufferLength)) {

            case 0: // occurs when invoking EMPTY_BLOB() function
                if (buffer != NULL) {
                    delete[] buffer;
                    buffer = 0;
                }
                logger.message("Empty element NOT inserted into the index: ROWID=" + string((char *) rowId));
                break;

            case 1:
            {
                obj->Unserialize((stByte *) buffer, bufferLength);
                delete[] buffer;
                buffer = 0;

                /* Set the rowid selected as a parameter as the OID of obj */
                try {
                    obj->SetStrOID(ROWID_SIZE, (stByte *) rowId);
                } catch (logic_error &exc) {
                    delete obj;
                    obj = 0;
                    delete slim;
                    slim = 0;
                    delete pmSlim;
                    pmSlim = 0;
                    raiseException(ctx, (text *) "SLM0041: Error setting Oracle rowid to the OID of index element");
                    return rval;
                }

                /* Add element to the tree */
                if (!slim->Add(obj)) {
                    delete obj;
                    obj = 0;
                    delete slim;
                    slim = 0;
                    delete pmSlim;
                    pmSlim = 0;
                    raiseException(ctx, (text *) "SLM-0043: Error inserting element into the index");
                    return rval;
                }

                logger.message("INS");
                break;
            }

            default:
            {
                if (buffer != NULL) {
                    delete[] buffer;
                    buffer = 0;
                }
                delete obj;
                obj = 0;
                delete slim;
                slim = 0;
                delete pmSlim;
                pmSlim = 0;
                raiseException(ctx, (text *) "SLM-0012: Error unserializing signature in the first argument");
                return rval;
            }

        } //end switch

        /* Fetch the next row */
        status = OCIStmtFetch2(stmthp, errhp, (ub4) 1, (ub2) OCI_FETCH_NEXT,
                (sb4) 0, (ub4) OCI_DEFAULT);
    }

    /* Free the LobLocator descriptor */
    if (checkerr(ctx, errhp, OCIDescriptorFree((void *) signature, (ub4) OCI_DTYPE_LOB)))
        return rval;

    /* Free the stmt handle */
    if (checkerr(ctx, errhp, OCIHandleFree((dvoid *) stmthp,
            (ub4) OCI_HTYPE_STMT)))
        return (rval);

    /* Optimize the tree with Slim-down */
    slim->Optimize();
    //    logger.message("Occup: " + toString(slim->GetMaxOccupation()));
    //    logger.message("Num obj: " + toString(slim->GetNumberOfObjects()));
    //    logger.message("Height: " + toString(slim->GetHeight()));
    /* OBS: O GetFatFactor deu erro na lib! Compila, mas o Oracle não aceita. */
    //    logger.message("Fat factor: " + toString(slim->GetFatFactor()));

    delete obj;
    obj = 0;
    delete slim;
    slim = 0;
    delete pmSlim;
    pmSlim = 0;

    /* Allocate memory for OCINumber first */
    rval = (OCINumber *) OCIExtProcAllocCallMemory(ctx, sizeof (OCINumber));

    /* Set up return code */
    if (checkerr(ctx, errhp, OCINumberFromInt(errhp, (dvoid *) & retval, sizeof (retval), OCI_NUMBER_SIGNED, rval)))
        return rval;

    logger.message("Index created: " + fileName);

    return rval;
} //end slimGenericIndexCreate

OCINumber * slimEuclideanIndexCreate(
        OCIExtProcContext *ctx,
        ODCIIndexInfo *ix,
        ODCIIndexInfo_ind *ix_ind,
        char *parms) {

    myBasicEuclideanMetricEvaluator *evaluator = new myBasicEuclideanMetricEvaluator();
    OCINumber *rval = (OCINumber *) 0;
    rval = slimGenericIndexCreate(ctx, ix, ix_ind, parms, evaluator);
    delete evaluator;
    return rval;
} //end slimEuclideanIndexCreate

OCINumber * slimManhattanIndexCreate(
        OCIExtProcContext *ctx,
        ODCIIndexInfo *ix,
        ODCIIndexInfo_ind *ix_ind,
        char *parms) {

    myBasicManhattanMetricEvaluator *evaluator = new myBasicManhattanMetricEvaluator();
    OCINumber *rval = (OCINumber *) 0;
    rval = slimGenericIndexCreate(ctx, ix, ix_ind, parms, evaluator);
    delete evaluator;
    return rval;
} //end slimManhattanIndexCreate

OCINumber * slimCanberraIndexCreate(
        OCIExtProcContext *ctx,
        ODCIIndexInfo *ix,
        ODCIIndexInfo_ind *ix_ind,
        char *parms) {

    myBasicCanberraMetricEvaluator *evaluator = new myBasicCanberraMetricEvaluator();
    OCINumber *rval = (OCINumber *) 0;
    rval = slimGenericIndexCreate(ctx, ix, ix_ind, parms, evaluator);
    delete evaluator;
    return rval;
} //end slimCanberraIndexCreate

OCINumber * slimIndexDrop(
        OCIExtProcContext *ctx,
        ODCIIndexInfo *ix,
        ODCIIndexInfo_ind *ix_ind) {

    OCIEnv *envhp = (OCIEnv *) 0; /* env. handle */
    OCISvcCtx *svchp = (OCISvcCtx *) 0; /* service handle */
    OCIError *errhp = (OCIError *) 0; /* error handle */

    int retval = (int) ODCI_SUCCESS; /* return from this function */
    OCINumber *rval = (OCINumber *) 0;

    /* Delete the index file */
    string fileName = DIRNAME + string((char *) OCIStringPtr(envhp, ix->IndexSchema)) + "." + string((char *) OCIStringPtr(envhp, ix->IndexName));
    if (remove(fileName.c_str()) != 0) {
        string msg = "SLM-0031: I/O error when deleting index file " + fileName;
        raiseException(ctx, (text *) msg.c_str());
        return rval;
    }

    /* Allocate memory for OCINumber first */
    rval = (OCINumber *) OCIExtProcAllocCallMemory(ctx, sizeof (OCINumber));

    /* Get the environment */
    if (checkerr(ctx, errhp, OCIExtProcGetEnv(ctx, &envhp, &svchp, &errhp)))
        return rval;

    /* Set up return code */
    if (checkerr(ctx, errhp, OCINumberFromInt(errhp, (dvoid *) & retval, sizeof (retval), OCI_NUMBER_SIGNED, rval)))
        return rval;

    logger.message("Index dropped: " + fileName);

    return rval;
} //end slimIndexDrop

OCINumber * slimGenericIndexInsert(
        OCIExtProcContext *ctx,
        ODCIIndexInfo *ix,
        ODCIIndexInfo_ind *ix_ind,
        char *rid,
        OCILobLocator *newval,
        myBasicMetricEvaluator *evaluator) {

    OCIEnv *envhp = (OCIEnv *) 0; /* env. handle */
    OCISvcCtx *svchp = (OCISvcCtx *) 0; /* service handle */
    OCIError *errhp = (OCIError *) 0; /* error handle */

    int retval = (int) ODCI_SUCCESS; /* return from this function */
    OCINumber *rval = (OCINumber *) 0;

    unsigned char * buffer = 0;
    unsigned int bufferLength = 0;

    myBasicArrayObject *obj;
    mySlimTree *slim;
    stDiskPageManager *pmSlim;

    /* Get the environment */
    if (checkerr(ctx, errhp, OCIExtProcGetEnv(ctx, &envhp, &svchp, &errhp)))
        return rval;

    //    switch (GetObjFromLOB(ctx, svchp, errhp, newval, obj)) {
    switch (readLOB(ctx, svchp, errhp, newval, &buffer, &bufferLength)) {

        case 0: // occurs when invoking EMPTY_BLOB() function
            if (buffer != NULL) {
                delete[] buffer;
                buffer = 0;
            }
            logger.message("Empty element NOT inserted into the index: ROWID=" + string((char *) rid));
            break;

        case 1:
        {
            /* Get the object stored on newval */
            obj = new myBasicArrayObject();
            obj->Unserialize(buffer, bufferLength);
            delete[] buffer;
            buffer = 0;

            /* Set the rowid given as a parameter as the OID of obj */
            try {
                obj->SetStrOID(ROWID_SIZE, (stByte *) rid);
            } catch (logic_error &exc) {
                raiseException(ctx, (text *) "SLM0041: Error setting Oracle rowid to the OID of index element");
                return rval;
            }

            /* Open the SlimTree from the index file */
            string fileName = DIRNAME + string((char *) OCIStringPtr(envhp, ix->IndexSchema)) + "." + string((char *) OCIStringPtr(envhp, ix->IndexName));
            try {
                pmSlim = new stDiskPageManager(fileName);
            } catch (io_error &e) {
                string msg = "SLM-0042: I/O error when opening index file " + fileName;
                raiseException(ctx, (text *) msg.c_str());
                return rval;
            }
            slim = new mySlimTree(pmSlim, evaluator);

            if (!slim->Add(obj)) {
                raiseException(ctx, (text *) "SLM-0043: Error inserting element into the index");
                return rval;
            }

            delete slim;
            slim = 0;
            delete pmSlim;
            pmSlim = 0;
            delete obj;
            obj = 0;
            break;
        }

        default:
        {
            raiseException(ctx, (text *) "SLM-0012: Error unserializing the signature inserted");
            return rval;
        }

    } //end switch

    /* Allocate memory for OCINumber first */
    rval = (OCINumber *) OCIExtProcAllocCallMemory(ctx, sizeof (OCINumber));

    /* Set up return code */
    if (checkerr(ctx, errhp, OCINumberFromInt(errhp, (dvoid *) & retval, sizeof (retval), OCI_NUMBER_SIGNED, rval)))
        return rval;

    logger.message("IndexInsert");

    return rval;
} //end slimGenericIndexInsert

OCINumber * slimEuclideanIndexInsert(
        OCIExtProcContext *ctx,
        ODCIIndexInfo *ix,
        ODCIIndexInfo_ind *ix_ind,
        char *rid,
        OCILobLocator *newval) {
    myBasicEuclideanMetricEvaluator *evaluator = new myBasicEuclideanMetricEvaluator();
    OCINumber *rval = (OCINumber *) 0;
    rval = slimGenericIndexInsert(ctx, ix, ix_ind, rid, newval, evaluator);
    delete evaluator;
    return rval;
} //end slimEuclideanIndexInsert

OCINumber * slimManhattanIndexInsert(
        OCIExtProcContext *ctx,
        ODCIIndexInfo *ix,
        ODCIIndexInfo_ind *ix_ind,
        char *rid,
        OCILobLocator *newval) {
    myBasicManhattanMetricEvaluator *evaluator = new myBasicManhattanMetricEvaluator();
    OCINumber *rval = (OCINumber *) 0;
    rval = slimGenericIndexInsert(ctx, ix, ix_ind, rid, newval, evaluator);
    delete evaluator;
    return rval;
} //end slimManhattanIndexInsert

OCINumber * slimCanberraIndexInsert(
        OCIExtProcContext *ctx,
        ODCIIndexInfo *ix,
        ODCIIndexInfo_ind *ix_ind,
        char *rid,
        OCILobLocator *newval) {
    myBasicCanberraMetricEvaluator *evaluator = new myBasicCanberraMetricEvaluator();
    OCINumber *rval = (OCINumber *) 0;
    rval = slimGenericIndexInsert(ctx, ix, ix_ind, rid, newval, evaluator);
    delete evaluator;
    return rval;
} //end slimCanberraIndexInsert

OCINumber * slimGenericIndexStart(
        OCIExtProcContext *ctx,
        Slimtree_im_type *self,
        Slimtree_im_type_ind *self_ind,
        ODCIIndexInfo *ix,
        ODCIIndexInfo_ind *ix_ind,
        ODCIPredInfo *predInfo,
        ODCIPredInfo_ind *predInfo_ind,
        ODCIQueryInfo *queryInfo,
        ODCIQueryInfo_ind *queryInfo_ind,
        OCINumber *start,
        short start_ind,
        OCINumber *stop,
        short stop_ind,
        OCILobLocator *queryElement,
        short queryElement_ind,
        myBasicMetricEvaluator *evaluator,
        string distanceName) {

    OCIEnv *envhp = (OCIEnv *) 0; /* env. handle */
    OCISvcCtx *svchp = (OCISvcCtx *) 0; /* service handle */
    OCIError *errhp = (OCIError *) 0; /* error handle */

    int retval = (int) ODCI_SUCCESS; /* return from this function */
    OCINumber *rval = (OCINumber *) 0;

    ub4 key; /* key value set in Slimtree_im_type->scanctx" */
    OCISession *usrhp = (OCISession *) 0; /* user handle */

    unsigned char * buffer;
    unsigned int bufferLength;

    myBasicArrayObject *obj;
    mySlimTree *slim;
    stDiskPageManager *pmSlim;
    myResult *result;
    stRowId *rset = (stRowId *) 0;
    unsigned int numRows = 0;

    unsigned short int flags;
    float outer_radius, inner_radius;

    /* Get the environment */
    if (checkerr(ctx, errhp, OCIExtProcGetEnv(ctx, &envhp, &svchp, &errhp)))
        return rval;

    /* get the user handle */
    if (checkerr(ctx, errhp, OCIAttrGet((dvoid *) svchp, (ub4) OCI_HTYPE_SVCCTX,
            (dvoid *) & usrhp, (ub4 *) 0,
            (ub4) OCI_ATTR_SESSION,
            errhp)))
        return rval;

    /* Get the bounds of the query */
    if (checkerr(ctx, errhp, OCINumberToInt(errhp, &predInfo->Flags, sizeof (flags), OCI_NUMBER_UNSIGNED,
            (dvoid *) & flags)))
        return rval;

    if (checkerr(ctx, errhp, OCINumberToReal(errhp, stop, sizeof (outer_radius), (dvoid *) & outer_radius)))
        return rval;

    if (checkerr(ctx, errhp, OCINumberToReal(errhp, start, sizeof (inner_radius), (dvoid *) & inner_radius)))
        return rval;

    /* Read the query element LOB content and perform the query */
    switch (readLOB(ctx, svchp, errhp, queryElement, &buffer, &bufferLength)) {

        case 0:
            raiseException(ctx, (text *) "SLM0052: Empty query element");
            return rval;

        case 1:
        {
            /* Get the object stored on queryElement */
            obj = new myBasicArrayObject();
            obj->Unserialize(buffer, bufferLength);
            delete[] buffer;
            buffer = 0;

            /* Open the SlimTree from the index file */
            string fileName = DIRNAME + string((char *) OCIStringPtr(envhp, ix->IndexSchema)) + "." + string((char *) OCIStringPtr(envhp, ix->IndexName));
            try {
                pmSlim = new stDiskPageManager(fileName);
            } catch (io_error &e) {
                string msg = "SLM-0053: I/O error when opening index file " + fileName;
                raiseException(ctx, (text *) msg.c_str());
                return rval;
            }
            slim = new mySlimTree(pmSlim, evaluator);

            /* Get the operator name */
            string Operator = (char *) OCIStringPtr(envhp, predInfo->ObjectName);

            if ((outer_radius < 0) || (inner_radius < 0)) {
                raiseException(ctx, (text *) "SLM0054: Thresholds must be greater than or equal to zero");
                return rval;
            }

            if (Operator.compare(distanceName + "_DIST") == 0) {
                if ((flags & ODCI_PRED_EXACT_MATCH) != 0) {
                    if ((inner_radius == 0) && (outer_radius == 0)) {
                        logger.message("point query");
                        result = slim->PointQuery(obj);
                    } else {
                        raiseException(ctx, (text *) "SLM0051: PointQuery variation not implemented on indextype");
                        return rval;
                    }
                } else {
                    if ((flags & ODCI_PRED_INCLUDE_STOP) != 0) {
                        if ((flags & ODCI_PRED_INCLUDE_START) == 0) {
                            if (outer_radius > 0) {
                                logger.message("range query");
                                result = slim->RangeQuery(obj, outer_radius);
                            } else {
                                logger.message("point query");
                                result = slim->PointQuery(obj);
                            }
                        } else {
                            logger.message("ring range query");
                            result = slim->RingQuery(obj, inner_radius, outer_radius);
                        }
                    } else if ((flags & ODCI_PRED_INCLUDE_START) == 0) {
                        if (inner_radius > 0) {
                            logger.message("reversed range query");
                            result = slim->ReversedRangeQuery(obj, inner_radius);
                        } else {
                            raiseException(ctx, (text *) "SLM0051: RangeQuery variation not implemented on indextype");
                            return rval;
                        }
                    } else {
                        raiseException(ctx, (text *) "SLM0051: ReversedRangeQuery variation not implemented on indextype");
                        return rval;
                    }
                }

            } else { // operator <distanceName>_kNN
                if (((flags & ODCI_PRED_INCLUDE_STOP) != 0) && ((flags & ODCI_PRED_EXACT_MATCH) == 0)) {
                    logger.message("kNN query");
                    result = slim->NearestQuery(obj, (stCount) outer_radius, false);
                } else {
                    raiseException(ctx, (text *) "SLM0051: kNNQuery variation not implemented on indextype");
                    return rval;
                }

            }

            /**
             * @todo Implement fetch greater than 2000 rows (Oracle default) for supporting next fetch calls
             */
            if (result->GetNumOfEntries() >= 2000) {
                numRows = 1999;
                logger.warning("SLM0059: Returning row count limit exceeded: max=1999 tuples. Returning the first 1999 rows of " + toString(result->GetNumOfEntries()));
            } else
                numRows = result->GetNumOfEntries();


            /* Allocate memory to hold the resultset */
            if (checkerr(ctx, errhp, OCIMemoryAlloc((dvoid *) usrhp, errhp,
                    (dvoid **) & rset,
                    OCI_DURATION_STATEMENT,
                    (ub4) ((numRows + 1) * ROWID_SIZE), // +1 to put a "NULL" rowid indicating the end of the resultset
                    OCI_MEMORY_CLEARED)))
                return rval;

            /* old stResult implementation
            Add to libslim.h: #define _DEPRECATED_VECTOR_RESULT

            for (stCount i = 0; i < numRows; i++) {
                myBasicArrayObject *tmpobj = (myBasicArrayObject *) (*result)[i].GetObject();
                memcpy((stByte *) rset[i], (stByte *) tmpobj->GetStrOID(), ROWID_SIZE);
            }
             */

            int i = 0;
            /* Get the result set limited to the row count limit (2000 tuples) */
            for (myResult::tItePairs it = result->beginPairs(); it != result->endPairs() && i < numRows; it++) {
                myBasicArrayObject *tmpobj = (myBasicArrayObject *) (*it)->GetObject();
                memcpy((stByte *) rset[i], (stByte *) tmpobj->GetStrOID(), ROWID_SIZE);
                /* Get the radius of the k-th element to perform a range query with k results */
                //if ((i==0) || (i==4) || (i==9) || (i==49) || (i==99))
                //  logger.message(toString(i+1) + "-NN " + toString((*it)->GetDistance()));
                i++;
            }

            stRowId nullRowId;
            memset(nullRowId, 0, ROWID_SIZE); // a "NULL" rowid to indicate the end of the resultset
            memcpy((stByte *) rset[numRows], (stByte *) nullRowId, ROWID_SIZE);

            delete result;
            result = 0;
            delete slim;
            slim = 0;
            delete pmSlim;
            pmSlim = 0;
            delete obj;
            obj = 0;
            break;
        }

        default:
            return rval;

    } //end switch

    /* Set index context to be returned */
    /* generate a key */
    if (checkerr(ctx, errhp, OCIContextGenerateKey((dvoid *) usrhp, errhp, &key)))
        return rval;

    /* set the memory address of the resultset to be saved in the context */
    if (checkerr(ctx, errhp, OCIContextSetValue((dvoid *) usrhp, errhp,
            OCI_DURATION_STATEMENT,
            (ub1 *) & key, (ub1)sizeof (key),
            (dvoid *) rset)))
        return rval;

    /* set the key as the attribute Slimtree_im_type->scanctx */
    if (checkerr(ctx, errhp, OCIRawAssignBytes(envhp, errhp, (ub1 *) & key, (ub4)sizeof (key), &(self->scanctx))))
        return rval;

    /* Indicate that neither the Slimtree_im_type self object nor the Slimtree_im_type->scanctx variable are NULL
     * See section "NULL Indicator Structure" on OCI Programming Guide page 11-22 and chapter 15 (OTT - Object Type Translator)
     */
    self_ind->_atomic = OCI_IND_NOTNULL;
    self_ind->scanctx = OCI_IND_NOTNULL;

    /* Allocate memory for OCINumber first */
    rval = (OCINumber *) OCIExtProcAllocCallMemory(ctx, sizeof (OCINumber));

    /* Set up return code */
    if (checkerr(ctx, errhp, OCINumberFromInt(errhp, (dvoid *) & retval, sizeof (retval), OCI_NUMBER_SIGNED, rval)))
        return rval;

    return rval;
} //end slimGenericIndexStart

OCINumber * slimEuclideanIndexStart(
        OCIExtProcContext *ctx,
        Slimtree_im_type *self,
        Slimtree_im_type_ind *self_ind,
        ODCIIndexInfo *ix,
        ODCIIndexInfo_ind *ix_ind,
        ODCIPredInfo *predInfo,
        ODCIPredInfo_ind *predInfo_ind,
        ODCIQueryInfo *queryInfo,
        ODCIQueryInfo_ind *queryInfo_ind,
        OCINumber *start,
        short start_ind,
        OCINumber *stop,
        short stop_ind,
        OCILobLocator *queryElement,
        short queryElement_ind) {

    myBasicEuclideanMetricEvaluator *evaluator = new myBasicEuclideanMetricEvaluator();
    OCINumber *rval = (OCINumber *) 0;
    rval = slimGenericIndexStart(ctx, self, self_ind, ix, ix_ind,
            predInfo, predInfo_ind, queryInfo, queryInfo_ind,
            start, start_ind, stop, stop_ind, queryElement, queryElement_ind,
            evaluator, "EUCLIDEAN");
    delete evaluator;
    return rval;
} //end slimEuclideanIndexStart

OCINumber * slimManhattanIndexStart(
        OCIExtProcContext *ctx,
        Slimtree_im_type *self,
        Slimtree_im_type_ind *self_ind,
        ODCIIndexInfo *ix,
        ODCIIndexInfo_ind *ix_ind,
        ODCIPredInfo *predInfo,
        ODCIPredInfo_ind *predInfo_ind,
        ODCIQueryInfo *queryInfo,
        ODCIQueryInfo_ind *queryInfo_ind,
        OCINumber *start,
        short start_ind,
        OCINumber *stop,
        short stop_ind,
        OCILobLocator *queryElement,
        short queryElement_ind) {

    myBasicManhattanMetricEvaluator *evaluator = new myBasicManhattanMetricEvaluator();
    OCINumber *rval = (OCINumber *) 0;
    rval = slimGenericIndexStart(ctx, self, self_ind, ix, ix_ind,
            predInfo, predInfo_ind, queryInfo, queryInfo_ind,
            start, start_ind, stop, stop_ind, queryElement, queryElement_ind,
            evaluator, "MANHATTAN");
    delete evaluator;
    return rval;
} //end slimManhattanIndexStart

OCINumber * slimCanberraIndexStart(
        OCIExtProcContext *ctx,
        Slimtree_im_type *self,
        Slimtree_im_type_ind *self_ind,
        ODCIIndexInfo *ix,
        ODCIIndexInfo_ind *ix_ind,
        ODCIPredInfo *predInfo,
        ODCIPredInfo_ind *predInfo_ind,
        ODCIQueryInfo *queryInfo,
        ODCIQueryInfo_ind *queryInfo_ind,
        OCINumber *start,
        short start_ind,
        OCINumber *stop,
        short stop_ind,
        OCILobLocator *queryElement,
        short queryElement_ind) {

    myBasicCanberraMetricEvaluator *evaluator = new myBasicCanberraMetricEvaluator();
    OCINumber *rval = (OCINumber *) 0;
    rval = slimGenericIndexStart(ctx, self, self_ind, ix, ix_ind,
            predInfo, predInfo_ind, queryInfo, queryInfo_ind,
            start, start_ind, stop, stop_ind, queryElement, queryElement_ind,
            evaluator, "CANBERRA");
    delete evaluator;
    return rval;
} //end slimCanberraIndexStart

OCINumber * slimIndexFetch(
        OCIExtProcContext *ctx,
        Slimtree_im_type *self,
        Slimtree_im_type_ind *self_ind,
        OCINumber *nrows,
        short nrows_ind,
        OCIArray **rids,
        short *rids_ind) {

    OCIEnv *envhp = (OCIEnv *) 0; /* env. handle */
    OCISvcCtx *svchp = (OCISvcCtx *) 0; /* service handle */
    OCIError *errhp = (OCIError *) 0; /* error handle */

    int retval = (int) ODCI_SUCCESS; /* return from this function */
    OCINumber *rval = (OCINumber *) 0;

    ub1 *key; /* pointer used in OCIRawPtr to retrieve the context */
    ub4 keylen;
    OCISession *usrhp = (OCISession *) 0; /* user handle */

    stRowId *rset;
    int nrowsval;
    OCIString *ridstr = (OCIString *) 0;

    /* Get the environment */
    if (checkerr(ctx, errhp, OCIExtProcGetEnv(ctx, &envhp, &svchp, &errhp)))
        return rval;

    /* get the user handle */
    if (checkerr(ctx, errhp, OCIAttrGet((dvoid *) svchp, (ub4) OCI_HTYPE_SVCCTX,
            (dvoid *) & usrhp, (ub4 *) 0,
            (ub4) OCI_ATTR_SESSION,
            errhp)))
        return rval;

    /* Retrieve context from key    */
    key = OCIRawPtr(envhp, self->scanctx);
    keylen = OCIRawSize(envhp, self->scanctx);

    if (checkerr(ctx, errhp, OCIContextGetValue((dvoid *) usrhp, errhp,
            key, (ub1) keylen,
            (dvoid **) &(rset))))
        return rval;

    /* get value of nrows */
    if (checkerr(ctx, errhp, OCINumberToInt(errhp, nrows, sizeof (nrowsval),
            OCI_NUMBER_SIGNED, (dvoid *) & nrowsval)))
        return rval;

    stRowId nullRowId;
    memset(nullRowId, 0, ROWID_SIZE);



    for (stCount i = 0; i < nrowsval; i++) {
        if (!memcmp(rset[i], nullRowId, ROWID_SIZE)) { //this "NULL" rowid indicates the end of the resultset
            short col_ind = OCI_IND_NULL; //indicates the end of the resultset
            /* have to create dummy oci string */
            if (checkerr(ctx, errhp, OCIStringAssignText(envhp, errhp, (text *) "dummy",
                    (ub2) 5, &ridstr)))
                return rval;

            /* append null element to collection */
            if (checkerr(ctx, errhp, OCICollAppend(envhp, errhp, (dvoid *) ridstr,
                    (dvoid *) & col_ind,
                    (OCIColl *) * rids)))
                return rval;
            break;
        } else {
            if (checkerr(ctx, errhp, OCIStringAssignText(envhp, errhp, (text *) rset[i],
                    (ub2) ROWID_SIZE, (OCIString **) & ridstr)))
                return rval;

            /* append rowid to collection */
            if (checkerr(ctx, errhp, OCICollAppend(envhp, errhp, (dvoid *) ridstr,
                    (dvoid *) 0, (OCIColl *) * rids)))
                return rval;
        }
    } //end for

    /* free ridstr finally */
    if (ridstr) {
        if (checkerr(ctx, errhp, OCIStringResize(envhp, errhp, (ub4) 0, &ridstr)))
            return rval;
    }

    /* Indicate that the collection rids is not NULL */
    *rids_ind = OCI_IND_NOTNULL;

    /* Allocate memory for OCINumber first */
    rval = (OCINumber *) OCIExtProcAllocCallMemory(ctx, sizeof (OCINumber));

    /* Set up return code */
    if (checkerr(ctx, errhp, OCINumberFromInt(errhp, (dvoid *) & retval, sizeof (retval), OCI_NUMBER_SIGNED, rval)))
        return rval;

    return rval;
} //end slimIndexFetch

OCINumber * slimIndexClose(
        OCIExtProcContext *ctx,
        Slimtree_im_type *self,
        Slimtree_im_type_ind *self_ind) {

    OCIEnv *envhp = (OCIEnv *) 0; /* env. handle */
    OCISvcCtx *svchp = (OCISvcCtx *) 0; /* service handle */
    OCIError *errhp = (OCIError *) 0; /* error handle */

    int retval = (int) ODCI_SUCCESS; /* return from this function */
    OCINumber *rval = (OCINumber *) 0;

    ub1 *key; /* pointer used in OCIRawPrt to retrieve the context */
    ub4 keylen;
    OCISession *usrhp = (OCISession *) 0; /* user handle */

    stRowId *rset;

    /* Get the environment */
    if (checkerr(ctx, errhp, OCIExtProcGetEnv(ctx, &envhp, &svchp, &errhp)))
        return rval;

    /* get the user handle */
    if (checkerr(ctx, errhp, OCIAttrGet((dvoid *) svchp, (ub4) OCI_HTYPE_SVCCTX,
            (dvoid *) & usrhp, (ub4 *) 0,
            (ub4) OCI_ATTR_SESSION,
            errhp)))
        return rval;

    /* Retrieve context from key    */
    key = OCIRawPtr(envhp, self->scanctx);
    keylen = OCIRawSize(envhp, self->scanctx);

    if (checkerr(ctx, errhp, OCIContextGetValue((dvoid *) usrhp, errhp,
            key, (ub1) keylen,
            (dvoid **) &(rset))))
        return rval;

    /* Free memory */
    if (checkerr(ctx, errhp, OCIMemoryFree((dvoid *) usrhp, errhp, (dvoid *) rset)))
        return rval;

    /* Allocate memory for OCINumber first */
    rval = (OCINumber *) OCIExtProcAllocCallMemory(ctx, sizeof (OCINumber));

    /* Set up return code */
    if (checkerr(ctx, errhp, OCINumberFromInt(errhp, (dvoid *) & retval, sizeof (retval), OCI_NUMBER_SIGNED, rval)))
        return rval;

    return rval;
} //end slimIndexClose