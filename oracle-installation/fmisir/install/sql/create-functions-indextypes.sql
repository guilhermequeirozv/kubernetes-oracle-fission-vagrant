-------------------
--FEATURE EXTRACTORS
-------------------

CREATE OR REPLACE FUNCTION  ReadSignatureFromFile (absoluteFileName VARCHAR2, signature IN OUT NOCOPY BLOB, numFeatures PLS_INTEGER, offset PLS_INTEGER)
RETURN PLS_INTEGER
AS EXTERNAL LIBRARY libfmisir NAME "readSignatureFromFile"
WITH CONTEXT;
/

CREATE OR REPLACE FUNCTION  GenerateSignature (image BLOB, signature IN OUT NOCOPY BLOB, extractor VARCHAR2, params VARCHAR2)
RETURN PLS_INTEGER
AS EXTERNAL LIBRARY libfmisir NAME "generateSignature"
WITH CONTEXT;
/

-------------------
--SLIM EUCLIDEAN TYPE
-------------------

CREATE OR REPLACE
FUNCTION  Euclidean_Distance (signature1 IN BLOB, signature2 IN BLOB)
RETURN FLOAT
AS LANGUAGE C LIBRARY libfmisir NAME "euclideanDistance"
WITH CONTEXT;
/

CREATE OR REPLACE OPERATOR Euclidean_Dist BINDING (BLOB, BLOB) RETURN FLOAT USING Euclidean_Distance;
/

CREATE OR REPLACE
FUNCTION  Euclidean_kNNq (signature1 IN BLOB, signature2 IN BLOB)
RETURN NUMBER IS 
BEGIN
  RAISE_APPLICATION_ERROR (-20000, 'SLM0050: the knn_Manhattan operator can only be used is a corresponding index exists on the attribute. Create the index or pose the knn query using a Window function.');
  RETURN ODCIConst.Success;
END Euclidean_kNNq;
/

CREATE OR REPLACE OPERATOR Euclidean_kNN BINDING (BLOB, BLOB) RETURN NUMBER USING Euclidean_kNNq;
/
/* Usage: WHERE Euclidean_kNN(sign, center_sign) <= 5;
 * Euclidean_kNN(sign, center_sign) <= 5 returns true for the tuples whose sign is IN kNN(euclidean, center, 5, no_tielist)
 */

CREATE OR REPLACE
TYPE slim_euclidean_im_type AS OBJECT
(
  scanctx RAW(4),
  STATIC FUNCTION ODCIGetInterfaces(ifclist OUT NOCOPY sys.ODCIObjectList) RETURN NUMBER,
  STATIC FUNCTION ODCIIndexCreate (ia sys.ODCIIndexInfo, parms VARCHAR2) RETURN NUMBER,
  STATIC FUNCTION ODCIIndexDrop(ia sys.ODCIIndexInfo) RETURN NUMBER,
  STATIC FUNCTION ODCIIndexInsert(ia sys.ODCIIndexInfo, rid VARCHAR2, newval BLOB) RETURN NUMBER,
  STATIC FUNCTION ODCIIndexDelete(ia sys.ODCIIndexInfo, rid VARCHAR2, oldval BLOB) RETURN NUMBER,
  STATIC FUNCTION ODCIIndexUpdate(ia sys.ODCIIndexInfo, rid VARCHAR2, oldval BLOB, newval BLOB) RETURN NUMBER,
  STATIC FUNCTION ODCIIndexStart(
                         self_ctx IN OUT NOCOPY slim_euclidean_im_type,
                         ia sys.ODCIIndexInfo,
                         op sys.ODCIPredInfo,
                         qi sys.ODCIQueryInfo,
                         strt NUMBER,
                         stop NUMBER,
                         cmpval BLOB) RETURN NUMBER,
  MEMBER FUNCTION ODCIIndexFetch(
                         SELF slim_euclidean_im_type,
                         nrows NUMBER,
                         rids OUT NOCOPY sys.ODCIRidList) RETURN NUMBER,
  MEMBER FUNCTION ODCIIndexClose(SELF slim_euclidean_im_type) RETURN NUMBER
);
/

CREATE OR REPLACE
TYPE BODY slim_euclidean_im_type IS

  STATIC FUNCTION ODCIGetInterfaces(ifclist OUT NOCOPY sys.ODCIObjectList) RETURN NUMBER IS
  BEGIN
    DBMS_OUTPUT.PUT_LINE('Slim GetInterfaces');
    ifclist := sys.ODCIObjectList(sys.ODCIObject('SYS','ODCIINDEX1'));
    RETURN ODCIConst.Success;
  END ODCIGetInterfaces;

    STATIC FUNCTION ODCIIndexCreate(ia sys.ODCIIndexInfo, parms VARCHAR2) RETURN NUMBER AS
    LANGUAGE C LIBRARY libfmisir NAME "slimEuclideanIndexCreate" WITH CONTEXT
    PARAMETERS (
        context,
        ia,
        ia indicator struct,
        parms,
        RETURN OCINumber
    );

    STATIC FUNCTION ODCIIndexDrop(ia sys.ODCIIndexInfo) RETURN NUMBER AS
    LANGUAGE C LIBRARY libfmisir NAME "slimIndexDrop" WITH CONTEXT
    PARAMETERS (
        context,
        ia,
        ia indicator struct,
        RETURN OCINumber
    );

    STATIC FUNCTION ODCIIndexInsert(ia sys.ODCIIndexInfo, rid VARCHAR2, newval BLOB) RETURN NUMBER AS
    LANGUAGE C LIBRARY libfmisir NAME "slimEuclideanIndexInsert" WITH CONTEXT
    PARAMETERS (
        context,
        ia,
        ia indicator struct,
        rid,
        newval,
        RETURN OCINumber
    );

  STATIC FUNCTION ODCIIndexDelete(ia sys.ODCIIndexInfo, rid VARCHAR2, oldval BLOB) RETURN NUMBER IS
  BEGIN
    DBMS_OUTPUT.PUT_LINE('Slim Delete');
    RETURN ODCIConst.Success;
  END ODCIIndexDelete;

  STATIC FUNCTION ODCIIndexUpdate(ia sys.ODCIIndexInfo, rid VARCHAR2, oldval BLOB, newval BLOB) RETURN NUMBER IS
  BEGIN
    DBMS_OUTPUT.PUT_LINE('Slim Update');
    RETURN ODCIConst.Success;
  END ODCIIndexUpdate;

  STATIC FUNCTION ODCIIndexStart(
                         self_ctx IN OUT NOCOPY slim_euclidean_im_type,
                         ia sys.ODCIIndexInfo,
                         op sys.ODCIPredInfo,
                         qi sys.ODCIQueryInfo,
                         strt NUMBER,
                         stop NUMBER,
                         cmpval BLOB) RETURN NUMBER AS
    LANGUAGE C LIBRARY libfmisir NAME "slimEuclideanIndexStart" WITH CONTEXT
    PARAMETERS (
        context,
        self_ctx,
        self_ctx indicator struct,
        ia,
        ia indicator struct,
        op,
        op indicator struct,
        qi,
        qi indicator struct,
        strt,
        strt indicator,
        stop,
        stop indicator,
        cmpval,
        cmpval indicator,
        RETURN OCINumber
    );                         

    MEMBER FUNCTION ODCIIndexFetch(
                         SELF slim_euclidean_im_type,
                         nrows NUMBER,
                         rids OUT NOCOPY sys.ODCIRidList) RETURN NUMBER AS
    LANGUAGE C LIBRARY libfmisir NAME "slimIndexFetch" WITH CONTEXT
    PARAMETERS (
        context,
        SELF,
        SELF indicator struct,
        nrows,
        nrows indicator,
        rids,
        rids indicator,
        RETURN OCINumber
    );                         


    MEMBER FUNCTION ODCIIndexClose(SELF slim_euclidean_im_type) RETURN NUMBER AS
    LANGUAGE C LIBRARY libfmisir NAME "slimIndexClose" WITH CONTEXT
    PARAMETERS (
        context,
        SELF,
        SELF indicator struct,
        RETURN OCINumber
    );        

END;
/

CREATE OR REPLACE INDEXTYPE slim_euclidean FOR 
Euclidean_Dist(BLOB, BLOB),
Euclidean_kNN(BLOB, BLOB)
USING slim_euclidean_im_type;
/

-------------------
--SLIM MANHATTAN TYPE
-------------------

CREATE OR REPLACE
FUNCTION  Manhattan_Distance (signature1 IN BLOB, signature2 IN BLOB)
RETURN FLOAT
AS LANGUAGE C LIBRARY libfmisir NAME "manhattanDistance"
WITH CONTEXT;
/

CREATE OR REPLACE OPERATOR Manhattan_Dist BINDING (BLOB, BLOB) RETURN FLOAT USING Manhattan_Distance;
/

CREATE OR REPLACE
FUNCTION  Manhattan_kNNq (signature1 IN BLOB, signature2 IN BLOB)
RETURN NUMBER IS 
BEGIN
  RAISE_APPLICATION_ERROR (-20000, 'SLM0050: the knn_Manhattan operator can only be used is a corresponding index exists on the attribute. Create the index or pose the knn query using a Window function.');
  RETURN ODCIConst.Success;
END Manhattan_kNNq;
/

CREATE OR REPLACE OPERATOR Manhattan_kNN BINDING (BLOB, BLOB) RETURN NUMBER USING Manhattan_kNNq;
/

/* Usage: WHERE Manhattan_kNN(sign, center_sign) <= 5;
 * Manhattan_kNN(sign, center_sign) <= 5 returns true for the tuples whose sign is IN kNN(manhattan, center, 5, no_tielist)
 */

CREATE OR REPLACE
TYPE slim_manhattan_im_type AS OBJECT
(
  scanctx RAW(4),
  STATIC FUNCTION ODCIGetInterfaces(ifclist OUT NOCOPY sys.ODCIObjectList) RETURN NUMBER,
  STATIC FUNCTION ODCIIndexCreate (ia sys.ODCIIndexInfo, parms VARCHAR2) RETURN NUMBER,
  STATIC FUNCTION ODCIIndexDrop(ia sys.ODCIIndexInfo) RETURN NUMBER,
  STATIC FUNCTION ODCIIndexInsert(ia sys.ODCIIndexInfo, rid VARCHAR2, newval BLOB) RETURN NUMBER,
  STATIC FUNCTION ODCIIndexDelete(ia sys.ODCIIndexInfo, rid VARCHAR2, oldval BLOB) RETURN NUMBER,
  STATIC FUNCTION ODCIIndexUpdate(ia sys.ODCIIndexInfo, rid VARCHAR2, oldval BLOB, newval BLOB) RETURN NUMBER,
  STATIC FUNCTION ODCIIndexStart(
                         self_ctx IN OUT NOCOPY slim_manhattan_im_type,
                         ia sys.ODCIIndexInfo,
                         op sys.ODCIPredInfo,
                         qi sys.ODCIQueryInfo,
                         strt NUMBER,
                         stop NUMBER,
                         cmpval BLOB) RETURN NUMBER,
  MEMBER FUNCTION ODCIIndexFetch(
                         SELF slim_manhattan_im_type,
                         nrows NUMBER,
                         rids OUT NOCOPY sys.ODCIRidList) RETURN NUMBER,
  MEMBER FUNCTION ODCIIndexClose(SELF slim_manhattan_im_type) RETURN NUMBER
);
/

CREATE OR REPLACE
TYPE BODY slim_manhattan_im_type IS

  STATIC FUNCTION ODCIGetInterfaces(ifclist OUT NOCOPY sys.ODCIObjectList) RETURN NUMBER IS
  BEGIN
    DBMS_OUTPUT.PUT_LINE('Slim GetInterfaces');
    ifclist := sys.ODCIObjectList(sys.ODCIObject('SYS','ODCIINDEX1'));
    RETURN ODCIConst.Success;
  END ODCIGetInterfaces;

    STATIC FUNCTION ODCIIndexCreate(ia sys.ODCIIndexInfo, parms VARCHAR2) RETURN NUMBER AS
    LANGUAGE C LIBRARY libfmisir NAME "slimManhattanIndexCreate" WITH CONTEXT
    PARAMETERS (
        context,
        ia,
        ia indicator struct,
        parms,
        RETURN OCINumber
    );

    STATIC FUNCTION ODCIIndexDrop(ia sys.ODCIIndexInfo) RETURN NUMBER AS
    LANGUAGE C LIBRARY libfmisir NAME "slimIndexDrop" WITH CONTEXT
    PARAMETERS (
        context,
        ia,
        ia indicator struct,
        RETURN OCINumber
    );

    STATIC FUNCTION ODCIIndexInsert(ia sys.ODCIIndexInfo, rid VARCHAR2, newval BLOB) RETURN NUMBER AS
    LANGUAGE C LIBRARY libfmisir NAME "slimManhattanIndexInsert" WITH CONTEXT
    PARAMETERS (
        context,
        ia,
        ia indicator struct,
        rid,
        newval,
        RETURN OCINumber
    );

  STATIC FUNCTION ODCIIndexDelete(ia sys.ODCIIndexInfo, rid VARCHAR2, oldval BLOB) RETURN NUMBER IS
  BEGIN
    DBMS_OUTPUT.PUT_LINE('Slim Delete');
    RETURN ODCIConst.Success;
  END ODCIIndexDelete;

  STATIC FUNCTION ODCIIndexUpdate(ia sys.ODCIIndexInfo, rid VARCHAR2, oldval BLOB, newval BLOB) RETURN NUMBER IS
  BEGIN
    DBMS_OUTPUT.PUT_LINE('Slim Update');
    RETURN ODCIConst.Success;
  END ODCIIndexUpdate;

  STATIC FUNCTION ODCIIndexStart(
                         self_ctx IN OUT NOCOPY slim_manhattan_im_type,
                         ia sys.ODCIIndexInfo,
                         op sys.ODCIPredInfo,
                         qi sys.ODCIQueryInfo,
                         strt NUMBER,
                         stop NUMBER,
                         cmpval BLOB) RETURN NUMBER AS
    LANGUAGE C LIBRARY libfmisir NAME "slimManhattanIndexStart" WITH CONTEXT
    PARAMETERS (
        context,
        self_ctx,
        self_ctx indicator struct,
        ia,
        ia indicator struct,
        op,
        op indicator struct,
        qi,
        qi indicator struct,
        strt,
        strt indicator,
        stop,
        stop indicator,
        cmpval,
        cmpval indicator,
        RETURN OCINumber
    );                         

    MEMBER FUNCTION ODCIIndexFetch(
                         SELF slim_manhattan_im_type,
                         nrows NUMBER,
                         rids OUT NOCOPY sys.ODCIRidList) RETURN NUMBER AS
    LANGUAGE C LIBRARY libfmisir NAME "slimIndexFetch" WITH CONTEXT
    PARAMETERS (
        context,
        SELF,
        SELF indicator struct,
        nrows,
        nrows indicator,
        rids,
        rids indicator,
        RETURN OCINumber
    );                         


    MEMBER FUNCTION ODCIIndexClose(SELF slim_manhattan_im_type) RETURN NUMBER AS
    LANGUAGE C LIBRARY libfmisir NAME "slimIndexClose" WITH CONTEXT
    PARAMETERS (
        context,
        SELF,
        SELF indicator struct,
        RETURN OCINumber
    );        

END;
/

CREATE OR REPLACE INDEXTYPE slim_manhattan FOR 
Manhattan_Dist(BLOB, BLOB),
Manhattan_kNN(BLOB, BLOB)
USING slim_manhattan_im_type;
/

-------------------
--SLIM CANBERRA TYPE
-------------------

CREATE OR REPLACE
FUNCTION  Canberra_Distance (signature1 IN BLOB, signature2 IN BLOB)
RETURN FLOAT
AS LANGUAGE C LIBRARY libfmisir NAME "canberraDistance"
WITH CONTEXT;
/

CREATE OR REPLACE OPERATOR Canberra_Dist BINDING (BLOB, BLOB) RETURN FLOAT USING Canberra_Distance;
/

CREATE OR REPLACE
FUNCTION  Canberra_kNNq (signature1 IN BLOB, signature2 IN BLOB)
RETURN NUMBER IS 
BEGIN
  RAISE_APPLICATION_ERROR (-20000, 'SLM0050: the knn_Manhattan operator can only be used is a corresponding index exists on the attribute. Create the index or pose the knn query using a Window function.');
  RETURN ODCIConst.Success;
END Canberra_kNNq;
/

CREATE OR REPLACE OPERATOR Canberra_kNN BINDING (BLOB, BLOB) RETURN NUMBER USING Canberra_kNNq;
/

/* Usage: WHERE Canberra_kNN(sign, center_sign) <= 5;
 * Canberra_kNN(sign, center_sign) <= 5 returns true for the tuples whose sign is IN kNN(canberra, center, 5, no_tielist)
 */

CREATE OR REPLACE
TYPE slim_canberra_im_type AS OBJECT
(
  scanctx RAW(4),
  STATIC FUNCTION ODCIGetInterfaces(ifclist OUT NOCOPY sys.ODCIObjectList) RETURN NUMBER,
  STATIC FUNCTION ODCIIndexCreate (ia sys.ODCIIndexInfo, parms VARCHAR2) RETURN NUMBER,
  STATIC FUNCTION ODCIIndexDrop(ia sys.ODCIIndexInfo) RETURN NUMBER,
  STATIC FUNCTION ODCIIndexInsert(ia sys.ODCIIndexInfo, rid VARCHAR2, newval BLOB) RETURN NUMBER,
  STATIC FUNCTION ODCIIndexDelete(ia sys.ODCIIndexInfo, rid VARCHAR2, oldval BLOB) RETURN NUMBER,
  STATIC FUNCTION ODCIIndexUpdate(ia sys.ODCIIndexInfo, rid VARCHAR2, oldval BLOB, newval BLOB) RETURN NUMBER,
  STATIC FUNCTION ODCIIndexStart(
                         self_ctx IN OUT NOCOPY slim_canberra_im_type,
                         ia sys.ODCIIndexInfo,
                         op sys.ODCIPredInfo,
                         qi sys.ODCIQueryInfo,
                         strt NUMBER,
                         stop NUMBER,
                         cmpval BLOB) RETURN NUMBER,
  MEMBER FUNCTION ODCIIndexFetch(
                         SELF slim_canberra_im_type,
                         nrows NUMBER,
                         rids OUT NOCOPY sys.ODCIRidList) RETURN NUMBER,
  MEMBER FUNCTION ODCIIndexClose(SELF slim_canberra_im_type) RETURN NUMBER
);
/

CREATE OR REPLACE
TYPE BODY slim_canberra_im_type IS

  STATIC FUNCTION ODCIGetInterfaces(ifclist OUT NOCOPY sys.ODCIObjectList) RETURN NUMBER IS
  BEGIN
    DBMS_OUTPUT.PUT_LINE('Slim GetInterfaces');
    ifclist := sys.ODCIObjectList(sys.ODCIObject('SYS','ODCIINDEX1'));
    RETURN ODCIConst.Success;
  END ODCIGetInterfaces;

    STATIC FUNCTION ODCIIndexCreate(ia sys.ODCIIndexInfo, parms VARCHAR2) RETURN NUMBER AS
    LANGUAGE C LIBRARY libfmisir NAME "slimCanberraIndexCreate" WITH CONTEXT
    PARAMETERS (
        context,
        ia,
        ia indicator struct,
        parms,
        RETURN OCINumber
    );

    STATIC FUNCTION ODCIIndexDrop(ia sys.ODCIIndexInfo) RETURN NUMBER AS
    LANGUAGE C LIBRARY libfmisir NAME "slimIndexDrop" WITH CONTEXT
    PARAMETERS (
        context,
        ia,
        ia indicator struct,
        RETURN OCINumber
    );

    STATIC FUNCTION ODCIIndexInsert(ia sys.ODCIIndexInfo, rid VARCHAR2, newval BLOB) RETURN NUMBER AS
    LANGUAGE C LIBRARY libfmisir NAME "slimCanberraIndexInsert" WITH CONTEXT
    PARAMETERS (
        context,
        ia,
        ia indicator struct,
        rid,
        newval,
        RETURN OCINumber
    );

  STATIC FUNCTION ODCIIndexDelete(ia sys.ODCIIndexInfo, rid VARCHAR2, oldval BLOB) RETURN NUMBER IS
  BEGIN
    DBMS_OUTPUT.PUT_LINE('Slim Delete');
    RETURN ODCIConst.Success;
  END ODCIIndexDelete;

  STATIC FUNCTION ODCIIndexUpdate(ia sys.ODCIIndexInfo, rid VARCHAR2, oldval BLOB, newval BLOB) RETURN NUMBER IS
  BEGIN
    DBMS_OUTPUT.PUT_LINE('Slim Update');
    RETURN ODCIConst.Success;
  END ODCIIndexUpdate;

  STATIC FUNCTION ODCIIndexStart(
                         self_ctx IN OUT NOCOPY slim_canberra_im_type,
                         ia sys.ODCIIndexInfo,
                         op sys.ODCIPredInfo,
                         qi sys.ODCIQueryInfo,
                         strt NUMBER,
                         stop NUMBER,
                         cmpval BLOB) RETURN NUMBER AS
    LANGUAGE C LIBRARY libfmisir NAME "slimCanberraIndexStart" WITH CONTEXT
    PARAMETERS (
        context,
        self_ctx,
        self_ctx indicator struct,
        ia,
        ia indicator struct,
        op,
        op indicator struct,
        qi,
        qi indicator struct,
        strt,
        strt indicator,
        stop,
        stop indicator,
        cmpval,
        cmpval indicator,
        RETURN OCINumber
    );                         

    MEMBER FUNCTION ODCIIndexFetch(
                         SELF slim_canberra_im_type,
                         nrows NUMBER,
                         rids OUT NOCOPY sys.ODCIRidList) RETURN NUMBER AS
    LANGUAGE C LIBRARY libfmisir NAME "slimIndexFetch" WITH CONTEXT
    PARAMETERS (
        context,
        SELF,
        SELF indicator struct,
        nrows,
        nrows indicator,
        rids,
        rids indicator,
        RETURN OCINumber
    );                         


    MEMBER FUNCTION ODCIIndexClose(SELF slim_canberra_im_type) RETURN NUMBER AS
    LANGUAGE C LIBRARY libfmisir NAME "slimIndexClose" WITH CONTEXT
    PARAMETERS (
        context,
        SELF,
        SELF indicator struct,
        RETURN OCINumber
    );        

END;
/

CREATE OR REPLACE INDEXTYPE slim_canberra FOR 
Canberra_Dist(BLOB, BLOB),
Canberra_kNN(BLOB, BLOB)
USING slim_canberra_im_type;
/

QUIT
