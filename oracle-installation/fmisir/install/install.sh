#!/bin/bash

echo "Installing FMI-SiR..."

echo "Registering FMI-SiR library on Oracle on schema $ORAUSER..."
$ORACLE_HOME/bin/sqlplus $ORAUSER/$ORAPWD @./sql/create-library.sql

echo "Creating FMI-SiR functions and indextypes on schema $ORAUSER..."
$ORACLE_HOME/bin/sqlplus $ORAUSER/$ORAPWD @./sql/create-functions-indextypes.sql

echo "Changing ownership of FMI-SiR directory..."
chown -R oracle.oinstall /opt/oracle/fmisir
if [ $? -eq 0 ]; then
  echo " Done."
else
  echo " WARNING: Could not change ownership. Oracle must be owner of the FMI-SiR directory."
fi

echo -n "Including the FMI-SiR library to that extproc is allowed to load..."
cat >> $ORACLE_HOME/hs/admin/extproc.ora << EOF
SET EXTPROC_DLLS=/opt/oracle/fmisir/libfmisir.so
EOF
echo " Done."


echo "Installation complete."

exit 0
