/* Copyright 2003-2017 GBDI-ICMC-USP <caetano@icmc.usp.br>
* 
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
* 
*   http://www.apache.org/licenses/LICENSE-2.0
* 
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
//---------------------------------------------------------------------------
// stHistogram.h ...
//
// Author: Marcos Rodrigues Vieira (mrvieira@icmc.usp.br)
//---------------------------------------------------------------------------
#include "stHistogram.h"
#include<cmath>
#include<cfloat>
//#include<iostream>

//---------------------------------------------------------------------------
// class stHistogram
//---------------------------------------------------------------------------
template <class ObjectType, class EvaluatorType>
stHistogram<ObjectType, EvaluatorType>::stHistogram(unsigned int numberOfBins){
   NumberOfBins = numberOfBins+1;
   Values = new double [NumberOfBins];
   Bins = new double [NumberOfBins];
   isNormal = false;
   collapsed = false;
}//end stHistogram

template <class ObjectType, class EvaluatorType>
stHistogram<ObjectType, EvaluatorType>::stHistogram(const stHistogram<ObjectType, EvaluatorType>& cHist){
	isNormal = cHist.isNormal;
	Objects = cHist.Objects;
	maximumdist = cHist.maximumdist;
	mean = cHist.mean;
	sdeviation = cHist.sdeviation;
	NumberOfBins = cHist.NumberOfBins;
	collapsed = cHist.collapsed;
        Values = new double [NumberOfBins];
        Bins = new double [NumberOfBins];
	for(int i = 0; i < NumberOfBins; i++){
		Values[i] = cHist.Values[i];
		Bins[i] = cHist.Bins[i];
	}
}

template <class ObjectType, class EvaluatorType>
stHistogram<ObjectType, EvaluatorType>::stHistogram(std::string filePath){   
   std::ifstream myHistogramFile(filePath);
   myHistogramFile >> NumberOfBins >> mean >> sdeviation >> maximumdist >> isNormal;
   //std:://cout << NumberOfBins << " " << mean << " " << sdeviation << std::endl;
   Values = new double [NumberOfBins];
   Bins = new double [NumberOfBins]; 
   int i = 0;
   while (i  < NumberOfBins) {
	myHistogramFile >> Values[i] >> Bins[i];
	//std:://cout << Bins[i] << " " << Values[i] << std::endl;
	i++;
   }
   myHistogramFile.close();
   collapsed = false;
   isNormal = false;
}//end stHistogram

//---------------------------------------------------------------------------
template <class ObjectType, class EvaluatorType>
stHistogram<ObjectType, EvaluatorType>::~stHistogram(){
   // Delete the resources.
   //delete[] Values;
   //delete[] Bins;
   // Delete the objects.
  /* for (unsigned int i = 0; i < Objects.size(); i++){
     delete Objects.at(i);
   }//end for*/
}//end ~stHistogram

template <class ObjectType, class EvaluatorType>
void stHistogram<ObjectType, EvaluatorType>::read(std::string filePath){   
   std::ifstream myHistogramFile(filePath);
   myHistogramFile >> NumberOfBins >> mean >> sdeviation >> maximumdist >> isNormal;
   //std:://cout << NumberOfBins << " " << mean << " " << sdeviation << std::endl;
   Values = new double [NumberOfBins];
   Bins = new double [NumberOfBins]; 
   int i = 0;
   while (i  < NumberOfBins) {
	myHistogramFile >> Values[i] >> Bins[i];
	//std:://cout << Bins[i] << " " << Values[i] << std::endl;
	i++;
   }
   myHistogramFile.close();
   collapsed = false;
}//end stHistogram

template <class ObjectType, class EvaluatorType>
void stHistogram<ObjectType, EvaluatorType>::clearHistogram(){
	delete Values;
	delete Bins;
	NumberOfBins = 0;
	mean = 0.0;
	sdeviation = 0.0;
	maximumdist = 0.0;
	isNormal = false;
	collapsed = false;
}
template <class ObjectType, class EvaluatorType>
void stHistogram<ObjectType, EvaluatorType>::copyHistogram(const stHistogram<ObjectType, EvaluatorType>& cHist){
	isNormal = cHist.isNormal;
	Objects = cHist.Objects;
	maximumdist = cHist.maximumdist;
	mean = cHist.mean;
	sdeviation = cHist.sdeviation;
	NumberOfBins = cHist.NumberOfBins;
	collapsed = cHist.collapsed;
        Values = new double [NumberOfBins];
        Bins = new double [NumberOfBins];
	for(int i = 0; i < NumberOfBins; i++){
		Values[i] = cHist.Values[i];
		Bins[i] = cHist.Bins[i];
	}
}
//---------------------------------------------------------------------------
template <class ObjectType, class EvaluatorType>
int stHistogram<ObjectType, EvaluatorType>::GetNumberOfObjects(){
   return Objects.size();
}//end GetNumberOfObjects

//---------------------------------------------------------------------------
template <class ObjectType, class EvaluatorType>
void stHistogram<ObjectType, EvaluatorType>::Build(EvaluatorType * metricEvaluator){
   unsigned int i, j, k;
   double maxDist, minDist, incDist;
   double ** dist;
   double tempDist;
   double count;
   unsigned int numberOfObjects;

   numberOfObjects = Objects.size();
   maxDist = 0;
   minDist = std::numeric_limits<double>::max();

   if (numberOfObjects > 0){
      // Allocate the matrix.
      dist = new double * [numberOfObjects];
      for (i = 0; i < numberOfObjects; i++){
         dist[i] = new double[numberOfObjects];
      }//end for

      // Build the distance matrix.
      for (i = 0; i < numberOfObjects; i++){
         // diagonal.
         dist[i][i] = 0;
         for (j = i + 1; j < numberOfObjects; j++){
            dist[i][j] = metricEvaluator->GetDistance(Objects[i], Objects[j]);
            dist[j][i] = dist[i][j];
            if (dist[i][j] > maxDist){
               maxDist = dist[i][j];
            }//end if
            if (dist[i][j] < minDist){
               minDist = dist[i][j];
            }//end if
         }//end for
      }//end for

      incDist = (maxDist - minDist)/this->NumberOfBins;

      // test if there is more than one object to build the matrix of distances.
      if (numberOfObjects > 2){
         // To measure the bins.
         for (i = 0; i < this->NumberOfBins; i++){
            count = 0;
            Bins[i] = minDist + incDist;
            minDist = Bins[i];
            for (j = 0; j < numberOfObjects - 1; j++){
               for (k = j + 1; k < numberOfObjects; k++){
                  if (dist[j][k] <= Bins[i]){
                     count++;
                  }//end if
               }//end for
            }//end for
            Values[i] = (double )count / (numberOfObjects * (numberOfObjects - 1) / 2.0);
         }//end for
      }else{
         for (i = 0; i < this->NumberOfBins; i++){
            Values[i] = 1;
            Bins[i] = maxDist;
         }//end for
      }//end if

      // clean.
      for (i = 0; i < numberOfObjects; i++){
         delete dist[i];
      }//end for
      delete[] dist;
   }else{
      //There is not any objects.
      for (i = 0; i < this->NumberOfBins; i++){
         Values[i] = 0;
         Bins[i] = std::numeric_limits<double>::max();
      }//end for
   }//end if
}//end Build

//---------------------------------------------------------------------------
template <class ObjectType, class EvaluatorType>
void stHistogram<ObjectType, EvaluatorType>::Add(ObjectType * obj){
   // Add a new object.
   Objects.insert(Objects.end(), obj->Clone());
}//end Add

//---------------------------------------------------------------------------
template <class ObjectType, class EvaluatorType>
double stHistogram<ObjectType, EvaluatorType>::GetBin(unsigned int idx){
   if (idx < NumberOfBins){
      return Bins[idx];
   }//end if
   return 0;
}//end GetBin

//---------------------------------------------------------------------------
template <class ObjectType, class EvaluatorType>
double stHistogram<ObjectType, EvaluatorType>::GetValue(unsigned int idx){
   if (idx < NumberOfBins){
      return Values[idx];
   }//end if
   return 0;
}//end GetValue

//---------------------------------------------------------------------------
#include<iostream>
using namespace std;
template <class ObjectType, class EvaluatorType>//busca binária
long double stHistogram<ObjectType, EvaluatorType>::GetArea(double limSup){
	int i = 0;
	long double differenceLimSup, differenceBins, differenceValues, reason;
	while(limSup > Bins[i]){ 
		i++; 
	}
	if(i>0){
		differenceLimSup = limSup - Bins[i-1];
		differenceValues = Values[i] - Values[i-1];
		differenceBins = Bins[i] - Bins[i-1];
		reason = differenceValues / differenceBins;
		reason = reason * differenceLimSup;
		reason =  reason + Values[i-1];
		reason = reason / Values[NumberOfBins-1];
		reason = reason * sqrt(Values[NumberOfBins-1]);
	}else{
		reason = Values[i] / Bins[i];
		reason = reason * limSup;
		reason = reason / Values[NumberOfBins-1];
		reason = reason * sqrt(Values[NumberOfBins-1]);
	}
	return reason;	
}


template <class ObjectType, class EvaluatorType>//busca binária
double stHistogram<ObjectType, EvaluatorType>::GetBinByValue(double value){
   unsigned int idx;

   idx = 0;
   while (idx < NumberOfBins){
      if (value < Bins[idx]){
         return Bins[idx];
      }//end if
      idx++;
   }//end while
   return Bins[NumberOfBins];
}//end GetValue


//---------------------------------------------------------------------------
template <class ObjectType, class EvaluatorType>
double stHistogram<ObjectType, EvaluatorType>::GetValueOfBin(double value){
   unsigned int idx;

   idx = 0;
   while (idx < NumberOfBins){
      if (value < Bins[idx]){
         return Values[idx];
      }//end if
      idx++;
   }//end while
   return Values[NumberOfBins];
}//end GetValue
//---------------------------------------------------------------------------
#include<iostream>
using namespace std;
template <class ObjectType, class EvaluatorType>
void stHistogram<ObjectType, EvaluatorType>::Build(EvaluatorType * metricEvaluator, double maxdist){
	maximumdist = maxdist;
	long double sum = 0, sqsum = 0;
	double dist;
	long double numberOfObjects = GetNumberOfObjects();
	for(int j = 0; j < NumberOfBins; j++){
		Bins[j] = (j+1)*(maxdist/(NumberOfBins-1));
		Values[j] = DBL_MIN;
	}	
	//std::ofstream myHistogramFile("dists");
	for(int i = 0; i < numberOfObjects; i++){
		for(int j = 0; j < numberOfObjects; j++){
			dist = metricEvaluator->GetDistance(*(Objects[i]), *(Objects[j]));
			//myHistogramFile << dist << endl;
			sum += dist;///(numberOfObjects*numberOfObjects);
			sqsum += dist*dist;///(numberOfObjects*numberOfObjects);
			if(dist >= maxdist){			
				Values[NumberOfBins-1] = Values[NumberOfBins-1] + DBL_MIN;
				if(dist > maximumdist){
					maximumdist = dist;
				}
			}else{
				for(int k = 0; k < NumberOfBins; k++){
					if(Bins[k] >= dist){
						Values[k] = Values[k] + DBL_MIN;
						break;
					}
				}
			}
		}
	}
	//myHistogramFile.close();
	
	Bins[NumberOfBins-1] = maximumdist;
	long double lnumberOfObjects = numberOfObjects*numberOfObjects;
	//cout << fixed <<"DIVIDING SUM BY NUMBER OF OBJECTS IN FULL LOOP: " << sum << "/" << lnumberOfObjects;
	mean = sum/lnumberOfObjects;///lnumberOfObjects;	
	//cout << " " << mean << endl;
	sdeviation = sqrt((sqsum-(sum*sum)/lnumberOfObjects)/(lnumberOfObjects-1));// / lnumberOfObjects - mean*mean;
	//cout << fixed << "mean and sd : " << mean << " " << sdeviation << endl;
	for(int i = 0; i < NumberOfBins; i++){
		Values[i] = Values[i] / DBL_MIN;
	}	
}
//---------------------------------------------------------------------------
double SqError(double *P, double *PP, int a, int b){
  double s1, s2;
  s2 = PP[b] - PP[a-1];
  s1 = P[b] - P[a-1];
  return (s2 - s1*s1/(b-a+1));
}
//---------------------------------------------------------------------------
template <class ObjectType, class EvaluatorType>
void stHistogram<ObjectType, EvaluatorType>::MakeVOptimal(int newNumberOfBins){
   double *P, *PP, *ValuesAcumulado, *BinsAcumulado, **BestErr;
   int i, j, k, *min_index;
   ValuesAcumulado = new double[newNumberOfBins];
   BinsAcumulado = new double[newNumberOfBins];
   P = new double[NumberOfBins+1];
   PP = new double[NumberOfBins+1];
   min_index = new int[NumberOfBins+1];
   BestErr = new double*[NumberOfBins+1];

   for(i = 0; i <= NumberOfBins; i++){
   	BestErr[i] = new double[NumberOfBins+1];
   }

   P[1] = Values[0];
   PP[1] = Values[0]*Values[0];
   for (i = 2; i <= NumberOfBins; i++){
     P[i] = P[i-1] + Values[i-1];
     PP[i] = PP[i-1] + (Values[i-1]*Values[i-1]);     
   }

   min_index[0] = 1;
   min_index[1] = 1;
  
   for (k = 1; k <= newNumberOfBins; k++){
	for (i = 1; i <= NumberOfBins; i++){
 		if ( k == 1 ){
 	       		BestErr[i][k] = SqError(P, PP, 1,i);
		}
		else{
			BestErr[i][k] = std::numeric_limits<double>::max();
			for (j = 1; j <= i-1; j++){
		   		if ( BestErr[j][k-1] + SqError(P, PP, j+1,i) < BestErr[i][k] ){
		       			BestErr[i][k] = BestErr[j][k-1] + SqError(P, PP, j+1,i);
					min_index[i]=j+1;
		   		}
			}
		}
	}
   }

   int index = newNumberOfBins-1, npoints, end_point;
   long double somatorio, spread;
   j = NumberOfBins;
   i = newNumberOfBins;
   while(i>=2){
        somatorio = 0;
        end_point = j;
	npoints = 0;
        j = min_index[j];
	//cout << "[" << j << " " << end_point << "]" << endl;
	////cout << "SUM " << endl;
        for(k = j-1; k < end_point; k++){
                //npoints++;
		////cout << Values[k] << " ";
		somatorio += Values[k];
	}
	////cout << endl;
	//spread = abs(Bins[end_point-1]-Bins[j-1]);
	////cout << "SPREAD IS: " << spread << endl;
	/*if(spread == 0){
		ValuesAcumulado[index] = somatorio;
	}else{
		ValuesAcumulado[index] = somatorio/spread;
	}*/
	ValuesAcumulado[index] = somatorio;
        BinsAcumulado[index] = Bins[end_point-1];
	j--;
	i--;
	index--;
  }

  somatorio = 0;
  //npoints = 0;
  //collapsed = true;
 /* spread = abs(Bins[index]-Bins[j-1]);
  ////cout << "spread: " << spread << endl;
  if(spread == 0){
	spread = 1;
  }*/
  for(k = 0; k < j; k++){
	////cout << "values " << Values[k] << endl;
	somatorio += Values[k];
	//npoints++;
  }
  ValuesAcumulado[index] = somatorio;///spread;
  //cout << "[" << 1 << " " << j << "]" << endl;
  
  //ValuesAcumulado[index] = somatorio/spread;
  BinsAcumulado[index] = Bins[j-1];

  for(int i = 1; i < newNumberOfBins; i++){	
	ValuesAcumulado[i] += ValuesAcumulado[i-1];
  }
  
  NumberOfBins = newNumberOfBins;
  Values = ValuesAcumulado;
  Bins = BinsAcumulado;
}
//---------------------------------------------------------------------------
template <class ObjectType, class EvaluatorType>
void stHistogram<ObjectType, EvaluatorType>::Persist(std::string fileName){
	std::ofstream myHistogramFile(fileName);
	myHistogramFile << std::fixed << NumberOfBins << " " << mean << " " << sdeviation << " " << maximumdist << " " << isNormal << std::endl;
	myHistogramFile << std::fixed << Values[0] << " " << Bins[0] << std::endl;
	for(int i = 1; i < NumberOfBins; i++){
		myHistogramFile << std::fixed << Values[i] << " " << Bins[i] << std::endl;
	}
	myHistogramFile << std::endl;
	myHistogramFile.close();
} 
//---------------------------------------------------------------------------
template <class ObjectType, class EvaluatorType>
void stHistogram<ObjectType, EvaluatorType>::Persist2(std::string fileName){
	std::ofstream myHistogramFile(fileName);
	myHistogramFile << 99999 << std::endl;
	myHistogramFile << 10 << std::endl;
	myHistogramFile << std::fixed << Values[0];
	int val = Values[0];
	for(int i = 1; i < NumberOfBins; i++){
		val += Values[i];
		myHistogramFile << std::fixed << " " << val;
	}
	myHistogramFile << std::endl;
	myHistogramFile << 99999 << std::endl;
	myHistogramFile.close();
} 
//---------------------------------------------------------------------------
template <class ObjectType, class EvaluatorType>
void stHistogram<ObjectType, EvaluatorType>::Collapse(){
	if(!collapsed){	
		for(int i = 1; i < NumberOfBins; i++){
			Values[i]+= Values[i-1];
		}
		collapsed = false;
	}
}
