/* Copyright 2003-2017 GBDI-ICMC-USP <caetano@icmc.usp.br>
* 
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
* 
*   http://www.apache.org/licenses/LICENSE-2.0
* 
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
//---------------------------------------------------------------------------
// stHistogram.h ...
//
// Author: Marcos Rodrigues Vieira (mrvieira@icmc.usp.br)
//---------------------------------------------------------------------------
#ifndef stHistogramH
#define stHistogramH

#include <vector>
#include <limits>
#include <string>
#include <iostream>
//---------------------------------------------------------------------------
// class stHistogram
//---------------------------------------------------------------------------
template <class ObjectType, class EvaluatorType>
class stHistogram {

   public:

      /**
      * Type of the object.
      */
      typedef ObjectType tObject;

      /**
      * Creates a new instance of this class.
      */
      stHistogram(unsigned int numberOfBins = 100);
      stHistogram(std::string filePath);
      stHistogram(const stHistogram<ObjectType, EvaluatorType>& cHist);

      /**
      * Destruct of this class.
      */
      ~stHistogram();

      int Size(){
         return NumberOfBins;
      }//end Size

      int GetNumberOfObjects();

      void Build(EvaluatorType * metricEvaluator);

      void Add(ObjectType * obj);

      double GetBin(unsigned int idx);

      double GetValue(unsigned int idx);

      double GetValueOfBin(double value);

      void Build(EvaluatorType * metricEvaluator, double maxdist);

      void MakeVOptimal(int newNumberOfBins);

      std::vector <tObject *> GetObjects(){ return Objects; }
      void setObjects(std::vector <tObject *> o){ Objects = o; }

      void Persist(std::string fileName);
      void Persist2(std::string fileName);
      double GetMean(){ return mean; }
      double GetSDeviation(){ return sdeviation; }
      double GetBinByValue(double value);
      double GetMaxDist(){ return maximumdist; }
      void Collapse();
      long double GetArea(double limSup);
      bool Normal(){ return isNormal; }
      void setNormal(bool normal){ isNormal = normal; }
      void read(std::string filePath);
      void clearHistogram();
      void copyHistogram(const stHistogram<ObjectType, EvaluatorType>& cHist);

   private:
      bool isNormal;

      std::vector <tObject *> Objects;

      //frequency of objects
      double * Values;

      //lower limit value
      double * Bins, maximumdist;
      
      double mean, sdeviation;

      unsigned int NumberOfBins;

      bool collapsed;

};//end stHistogram

#include "stHistogram-inl.h"

#endif //end stHistogramH
