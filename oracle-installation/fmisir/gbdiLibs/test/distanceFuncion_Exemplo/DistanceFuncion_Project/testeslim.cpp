//--------------------- c/c++ include ---------------------------------------
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <ctime>
#include <cstdlib>
#include <fstream>

//--- arboretum src include to avoid having to build the library ---
#include <util/stUtil.cpp>
#include <arboretum/stTreeInformation.cpp>
#include <arboretum/storage/stPage.cpp>

// includes for stPlainDiskPageManager
#include <arboretum/storage/stPlainDiskPageManager.cpp>

// includes for stDiskPageManager ==> page manager with memory cache
#include <arboretum/storage/stDiskPageManager.cpp>
#include <arboretum/storage/CStorage.cpp>

// includes for stMemoryPageManager
#include <arboretum/storage/stMemoryPageManager.cpp>

// includes for slimTree
#include <arboretum/slimTree/stSlimNode.cpp>

// includes for dummyTree
//#include <stDummyNode.cpp>

// includes for MTree
//#include <stMNode.cpp>

//--------------------- arboretum header include ----------------------------
//#include <gbdi/datatype/stBasicObjects.h>
#include <gbdi/datatype/byteOIDArrayObject.h>

//#include <arboretum/stUserLayerUtil.h>
#include <gbdi/distance/stMetricEvaluators.h>
#include <gbdi/arboretum/slimTree/stSlimTree.h>

// Page managers
#include <gbdi/arboretum/storage/stPlainDiskPageManager.h>
#include <gbdi/arboretum/storage/stDiskPageManager.h>
#include <gbdi/arboretum/storage/stMemoryPageManager.h>

//--------------------- Slim and R type declarations ------------------------
//typedef stBasicArrayObject <double, int> myBasicArrayObject;
typedef ByteOIDArrayObject <float> myBasicArrayObject;

typedef stEuclideanMetricEvaluator < float > myBasicMetricEvaluator;
typedef stResult < myBasicArrayObject > myResult;

//--------------------- Slim type declarations ------------------------------
typedef stSlimTree < myBasicArrayObject, myBasicMetricEvaluator > mySlimTree;

//--------------------- main ------------------------------------------------
int main(int argc, char** argv) {
    // read data
    int objectCount = 0;
    char *cityName;
    float *latlon = new float[2];
    char **cityNames = new char*[5507];
    myBasicArrayObject **allCities = new myBasicArrayObject*[5507];
    ifstream in("cities.txt");
    if (!in.is_open()) {
        cout << "Cannot open data file!" << endl;
        return -1;
    }
    else {
        cityName = new char[200];
        while (in.getline(cityName, 200, '\t')){
            in >> latlon[0];
            in >> latlon[1];
            in.ignore();
            cityNames[objectCount] = cityName;
            allCities[objectCount] = new myBasicArrayObject(2, latlon, strlen(cityName)+1, (stByte *) cityName);
            cityName = new char[200];
            objectCount++;
        }
        in.close();
    }

    // Slim-tree test
    cout << "Building Slim-tree... " << endl;
    //stPlainDiskPageManager *pmSlim = new stPlainDiskPageManager("slim.dat", 1024);
    stDiskPageManager *pmSlim = new stDiskPageManager("slim.dat", 1024);
    //stMemoryPageManager *pmSlim = new stMemoryPageManager(1024); // <== Nao funcionou o slim->Add com o stMemoryPageManager; nao encontrou o header no SlimNode::CreateNode
    mySlimTree *slim = new mySlimTree(pmSlim);

    for (int i = 0; i < objectCount; i++) {
        slim->Add(allCities[i]);
    }
    cout << "number of nodes=" << slim->GetNodeCount() << " height=" << slim->GetHeight() << endl;

    int queryobjectid = 3183; //Sao Carlos-SP

    // reset statistics
    slim->GetMetricEvaluator()->ResetStatistics();
    slim->GetPageManager()->ResetStatistics();

    // range query
    cout << "Query object=" << cityNames[queryobjectid] << " lat=" << *((float *) allCities[queryobjectid]->Get(0)) << " long=" << *((float *) allCities[queryobjectid]->Get(1)) << endl;
    myResult *result = slim->RangeQuery(allCities[queryobjectid], 0.3);
    cout << "Range query result: " << endl;
    cout << "distancecount=" << slim->GetMetricEvaluator()->GetDistanceCount() << " diskaccesses=" << slim->GetPageManager()->GetAccessCount() << endl;
    int i=0;
    for (myResult::tItePairs it = result->beginPairs(); it != result->endPairs(); it++){
         myBasicArrayObject *tmpobj = (myBasicArrayObject *)(*it)->GetObject();
         cout << "[" << ++i << "] (" << *((float *) allCities[queryobjectid]->Get(0)) << ", " << *((float *) allCities[queryobjectid]->Get(1)) << ") " << (char *) tmpobj->GetStrOID() << " (L2=" << (stDistance)(*it)->GetDistance() << ")" << endl;
    }
    delete result;

    // reset statistics
    slim->GetMetricEvaluator()->ResetStatistics();
    slim->GetPageManager()->ResetStatistics();

    // knn query
    cout << "Query object=" << cityNames[queryobjectid] << " lat=" << *((float *) allCities[queryobjectid]->Get(0)) << " long=" << *((float *) allCities[queryobjectid]->Get(1)) << endl;
    result = slim->NearestQuery(allCities[queryobjectid], 10);
    cout << "kNN query result: " << endl;
    cout << "distancecount=" << slim->GetMetricEvaluator()->GetDistanceCount() << " diskaccesses=" << slim->GetPageManager()->GetAccessCount() << endl;
    i=0;
    for (myResult::tItePairs it = result->beginPairs(); it != result->endPairs(); it++){
         myBasicArrayObject *tmpobj = (myBasicArrayObject *)(*it)->GetObject();
         cout << "[" << ++i << "] (" << *((float *) allCities[queryobjectid]->Get(0)) << ", " << *((float *) allCities[queryobjectid]->Get(1)) << ") " << (char *) tmpobj->GetStrOID() << " (L2=" << (stDistance)(*it)->GetDistance() << ")" << endl;
    }
    delete result;



    // delete tree
    delete slim;
    delete pmSlim;
/*
    // R-tree test
    printf("\nBuilding R-tree... ");
    myBasicMetricEvaluator *me = new myBasicMetricEvaluator();
    stPlainDiskPageManager *pmR = new stPlainDiskPageManager("r.dat", 1024);
    myRTree *rtree = new myRTree(pmR);
    rtree->SetQueryMetricEvaluator(me);
    rtree->SetSplitMethod(myRTree::smQUADRATIC); // smLINEAR, smQUADRATIC, smEXPONENTIAL

    for (int i = 0; i < objectCount; i++) {
        rtree->Add(allCities[i]);
    }
    printf("number of nodes=%d height=%d\n",rtree->GetNodeCount(),rtree->GetHeight());

    // reset statistics
    rtree->GetQueryMetricEvaluator()->ResetStatistics();
    rtree->GetPageManager()->ResetStatistics();

    // range query
    printf("\nQuery object=%s lat=%f long=%f", cityNames[queryobjectid], allCities[queryobjectid]->Get(0), allCities[queryobjectid]->Get(1));
    result = rtree->RangeQuery(allCities[queryobjectid], 0.3);
    printf("\nRange query result: ");
    printf("distancecount=%d diskaccesses=%d\n", rtree->GetQueryMetricEvaluator()->GetDistanceCount(), rtree->GetPageManager()->GetAccessCount());
    for (stCount i = 0; i < result->GetNumOfEntries(); i++) {
        myBasicArrayObject *tmpobj = (myBasicArrayObject *)(*result)[i].GetObject();
        printf("[%d] %s\n",i, cityNames[tmpobj->GetOID()]);
    }
    delete result;

    // reset statistics
    rtree->GetQueryMetricEvaluator()->ResetStatistics();
    rtree->GetPageManager()->ResetStatistics();

    // knn query
    printf("\nQuery object=%s lat=%f long=%f", cityNames[queryobjectid], allCities[queryobjectid]->Get(0), allCities[queryobjectid]->Get(1));
    result = rtree->NearestQuery(allCities[queryobjectid], 10);
    printf("\nkNN query result: ");
    printf("distancecount=%d diskaccesses=%d\n", rtree->GetQueryMetricEvaluator()->GetDistanceCount(), rtree->GetPageManager()->GetAccessCount());
    for (stCount i = 0; i < result->GetNumOfEntries(); i++) {
        myBasicArrayObject *tmpobj = (myBasicArrayObject *)(*result)[i].GetObject();
        printf("[%d] %s\n",i, cityNames[tmpobj->GetOID()]);
    }
    delete result;

    // delete tree
    delete rtree;
    delete pmR;
*/
    return (EXIT_SUCCESS);
}
