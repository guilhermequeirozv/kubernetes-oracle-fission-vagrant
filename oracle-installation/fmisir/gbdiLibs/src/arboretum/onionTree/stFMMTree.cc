/**********************************************************************
* GBDI Arboretum - Copyright (c) 2002-2004 GBDI-ICMC-USP
*
*                           Homepage: http://gbdi.icmc.usp.br/arboretum
**********************************************************************/
/* ====================================================================
 * The GBDI-ICMC-USP Software License Version 1.0
 *
 * Copyright (c) 2004 Grupo de Bases de Dados e Imagens, Instituto de
 * Ci�ncias Matem�ticas e de Computa��o, University of S�o Paulo -
 * Brazil (the Databases and Image Group - Intitute of Matematical and 
 * Computer Sciences).  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:
 *       "This product includes software developed by Grupo de Bases
 *        de Dados e Imagens, Instituto de Ci�ncias Matem�ticas e de
 *        Computa��o, University of S�o Paulo - Brazil (the Databases 
 *        and Image Group - Intitute of Matematical and Computer 
 *        Sciences)"
 *
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names of the research group, institute, university, authors
 *    and collaborators must not be used to endorse or promote products
 *    derived from this software without prior written permission.
 *
 * 5. The names of products derived from this software may not contain
 *    the name of research group, institute or university, without prior
 *    written permission of the authors of this software.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OF THIS SOFTWARE OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * ====================================================================
 *                                            http://gbdi.icmc.usp.br/
 */

/**
* @file
*
* This file is the implementation of the FMM-tree.
*
* @version 1.0
* $Date: 2008/05/19$
* @author: Ives Ren� Venturini Pola (ives@icmc.usp.br) $
* @author: Caio C�sar Mori Car�lo (ccarelo@icmc.usp.br) $
*/
// Copyright (c) 2008 GBDI-ICMC-USP
#include "stFMMTree.h"
template <class ObjectType, class EvaluatorType>
stFMMTree<ObjectType, EvaluatorType>::stFMMTree(const unsigned& cuReplacementType):stMetricAccessMethod<ObjectType, EvaluatorType>(new EvaluatorType()){
    Root = 0;
    uTypeOfExpansion = 0;
    uFixedNumberOfExpansions = 0;
    uReplacementType = cuReplacementType;
}

template <class ObjectType, class EvaluatorType>
stFMMTree<ObjectType, EvaluatorType>::stFMMTree(const unsigned& cuReplacementType, const unsigned& cuFixedNumberOfExpansions):stMetricAccessMethod<ObjectType, EvaluatorType>(new EvaluatorType()){
    Root = 0;
    uTypeOfExpansion = 1;
    uFixedNumberOfExpansions = cuFixedNumberOfExpansions;
    uReplacementType = cuReplacementType;
}

template <class ObjectType, class EvaluatorType>
stFMMTree<ObjectType, EvaluatorType>::~stFMMTree(){
    if (Root != 0) {
        delete Root;
        Root = 0;
    }
}

template <class ObjectType, class EvaluatorType>
bool stFMMTree<ObjectType, EvaluatorType>::Add(tObject* Element){
   Insert(Root, Root, Element->Clone());
   return true;
}

template <class ObjectType, class EvaluatorType>
stResult<ObjectType>* stFMMTree<ObjectType, EvaluatorType>::RangeQuery(tObject* sample, stDistance range){
    if (Root == 0){
      return NULL;
    }
    
    tResult* Result = new tResult();
    //Result->SetQueryInfo(sample->Clone(), tResult::RANGEQUERY, -1, range, false);
    Range(Root, sample, range, Result);

    return Result;
    
}

template <class ObjectType, class EvaluatorType>
stResult<ObjectType>* stFMMTree<ObjectType, EvaluatorType>::NearestQuery(tObject* sample, int k, bool tie){

    if (Root == 0){
        return NULL;
    }

    tResult* Result = new tResult();
    //Result->SetQueryInfo(sample->Clone(), tResult::KNEARESTQUERY, k, -1.0, tie);
    stDistance RangeK = MAXDOUBLE;
    NearestGuided(Root, sample, k, RangeK, tie, Result);

    return Result;

}

template <class ObjectType, class EvaluatorType>
void stFMMTree<ObjectType, EvaluatorType>::CreateSubspaces(tFMMNode*& Father, tFMMNode*& Node) {
    if (uTypeOfExpansion == 1) {
        Node->uExpansion = uFixedNumberOfExpansions;
    }
    else {
        if (Node->r < Father->r / 2.0) {
                Node->uExpansion = (unsigned)(Father->r / Node->r);
                Node->uExpansion++;
        }
    }
    Node->uSon = (Node->uExpansion * 3) + 4;
    Node->Son = new tFMMNode*[Node->uSon];
    for (unsigned i = 0; i < Node->uSon; i++) {
        Node->Son[i] = 0;
    }
}

template <class ObjectType, class EvaluatorType>
unsigned stFMMTree<ObjectType, EvaluatorType>::ChooseSubspace(tFMMNode*& Node, const stDistance& cdFirst, const stDistance& cdSecond) {

    unsigned uExpansion;
    unsigned uRegion = 0;
    stDistance R = 0.0;

    for (uExpansion = 0; uExpansion <= Node->uExpansion; uExpansion++) {
        R += Node->r;
        if (cdFirst < R && cdSecond < R) { break; }
        if (cdFirst < R && cdSecond >= R) { uRegion = 1; break; }
        if (cdFirst >= R && cdSecond < R) { uRegion = 2; break; }
    }

    return (uExpansion * 3) + uRegion;

}

template <class ObjectType, class EvaluatorType>
void stFMMTree<ObjectType, EvaluatorType>::Replacement(tFMMNode*& Father, tFMMNode*& Node, tObject*& Element, stDistance& dFirst, stDistance& dSecond) {

    int iRand;
    double DR, D1, D2, dTemp;
    ObjectType* Temp;

    switch (uReplacementType) {
            
        case rtMIN:

            if (dFirst < dSecond) {
                if (dFirst < Node->r) {
                    //replace si and s2
                    Temp = Node->Second;
                    Node->Second = Element;
                    Element = Temp;
                    //replace d1 and r
                    dTemp = dFirst;
                    dFirst = Node->r;
                    Node->r = dTemp;
                }
            }
            else {
                if (dSecond < Node->r) {
                    //replace si and s1
                    Temp = Node->First;
                    Node->First = Element;
                    Element = Temp;
                    //replace d2 and r
                    dTemp = dSecond;
                    dSecond = Node->r;
                    Node->r = dTemp;
                }
            }

            break;
            
        case rtMAX:
            
            if (dFirst > dSecond) {
                if (dFirst > Node->r) {
                    //replace si and s2
                    Temp = Node->Second;
                    Node->Second = Element;
                    Element = Temp;
                    //replace d1 and r
                    dTemp = dFirst;
                    dFirst = Node->r;
                    Node->r = dTemp;
                }
            }
            else {
                if (dSecond > Node->r) {
                    //replace si and s1
                    Temp = Node->First;
                    Node->First = Element;
                    Element = Temp;
                    //replace d2 and r
                    dTemp = dSecond;
                    dSecond = Node->r;
                    Node->r = dTemp;
                }
            }
            
            break;
            
        case rtLIKEDAD:
            
            DR = fabs(Node->r - Father->r);
            D1 = fabs(dFirst - Father->r);
            D2 = fabs(dSecond - Father->r);

            if (D1 < D2) {
                if (D1 < DR) {
                    //replace si and s2
                    Temp = Node->Second;
                    Node->Second = Element;
                    Element = Temp;
                    //replace d1 and r
                    dTemp = dFirst;
                    dFirst = Node->r;
                    Node->r = dTemp;
                }
            }
            else {
                if (D2 < DR) {
                    //replace si and s1
                    Temp = Node->First;
                    Node->First = Element;
                    Element = Temp;
                    //replace d2 and r
                    dTemp = dSecond;
                    dSecond = Node->r;
                    Node->r = dTemp;
                }
            }

            
            break;
            
        case rtKEEPSMALL:

            DR = fabs(Node->r - (Father->r / 2.0));
            D1 = fabs(dFirst - (Father->r / 2.0));
            D2 = fabs(dSecond - (Father->r / 2.0));

            if (D1 < D2) {
                if (D1 < DR) {
                    //replace si and s2
                    Temp = Node->Second;
                    Node->Second = Element;
                    Element = Temp;
                    //replace d1 and r
                    dTemp = dFirst;
                    dFirst = Node->r;
                    Node->r = dTemp;
                }
            }
            else {
                if (D2 < DR) {
                    //replace si and s1
                    Temp = Node->First;
                    Node->First = Element;
                    Element = Temp;
                    //replace d2 and r
                    dTemp = dSecond;
                    dSecond = Node->r;
                    Node->r = dTemp;
                }
            }

            break;
            
        case rtRANDOM:
            
            iRand = rand() % 3;
            
            switch (iRand) {
                case 1:
                    //replace si and s2
                    Temp = Node->Second;
                    Node->Second = Element;
                    Element = Temp;
                    //replace d1 and r
                    dTemp = dFirst;
                    dFirst = Node->r;
                    Node->r = dTemp;
                    break;
                case 2:
                    //replace si and s1
                    Temp = Node->First;
                    Node->First = Element;
                    Element = Temp;
                    //replace d2 and r
                    dTemp = dSecond;
                    dSecond = Node->r;
                    Node->r = dTemp;
                    break;
            } 
            
            break;            

        case rtNOREPLACEMENT:
            
            break;

    }

}

template <class ObjectType, class EvaluatorType>
void stFMMTree<ObjectType, EvaluatorType>::Insert(tFMMNode*& Father, tFMMNode*& Node, tObject* Element){
    if (Node == 0) {
        //folha vazia
        Node = new tFMMNode();
        Node->First = Element;
    }
    else {
        if (Node->Second == 0) {
            //folha nao cheia
            Node->Second = Element;
        }
        else {
            //interno
            stDistance dFirst = this->myMetricEvaluator->GetDistance(Node->First, Element);
            stDistance dSecond = this->myMetricEvaluator->GetDistance(Node->Second, Element);
            //se for n� folha
            if (Node->Son == 0) {
                Node->r = this->myMetricEvaluator->GetDistance(Node->First, Node->Second);
                Replacement(Father, Node, Element, dFirst, dSecond);
                CreateSubspaces(Father, Node);
            }
            Insert(Node, Node->Son[ChooseSubspace(Node, dFirst, dSecond)], Element);
        }
    }
}
//#include <iostream>
template <class ObjectType, class EvaluatorType>
void stFMMTree<ObjectType, EvaluatorType>::Range(tFMMNode*& Node, tObject* Element, const stDistance& r, tResult* Result) {
    if (Node == 0) return;
    
    stDistance dFirst, dSecond;
    
    if (Node->First != 0) {
        //test the first pivot
        dFirst = this->myMetricEvaluator->GetDistance(Node->First, Element);
        if (r >= dFirst) {
            Result->AddPair(Node->First->Clone(), dFirst);
        }
        
        if (Node->Second != 0) {
            //test the second pivot
            dSecond = this->myMetricEvaluator->GetDistance(Node->Second, Element);
            if (r >= dSecond) {
                Result->AddPair(Node->Second->Clone(), dSecond);
            }
            
            //test the regions
            if (Node->Son != 0) {
                unsigned uExpansion;
                stDistance R = 0.0;
                for (uExpansion = 0; uExpansion <= Node->uExpansion; uExpansion++) {
                    if ((dFirst + r >= R) && (dSecond + r >= R)) {
                        R += Node->r;
                        if ((dFirst < R + r) && (dSecond < R + r)) Range(Node->Son[uExpansion * 3], Element, r, Result);
                        if ((dFirst < R + r) && (dSecond + r >= R)) Range(Node->Son[(uExpansion * 3) + 1], Element, r, Result);
                        if ((dFirst + r >= R) && (dSecond < R + r)) Range(Node->Son[(uExpansion * 3) + 2], Element, r, Result);
                    }
                }
                if ((dFirst + r >= R) && (dSecond + r >= R)) Range(Node->Son[uExpansion * 3], Element, r, Result);
            }
            
        }
        
    }
}

template <class ObjectType, class EvaluatorType>
void stFMMTree<ObjectType, EvaluatorType>::NearestGuided(tFMMNode*& Node, tObject* Element, const unsigned& k, stDistance& RangeK, const bool& bTie, tResult* Result) {

    if (Node == 0) return;

    stDistance dFirst, dSecond;

    //test the first element
    dFirst = this->myMetricEvaluator->GetDistance(Node->First, Element);
    if (Result->GetNumOfEntries() < k) {
        Result->AddPair(Node->First->Clone(), dFirst);
    }
    else {
        if (dFirst < Result->GetMaximumDistance()) {
            Result->AddPair(Node->First->Clone(), dFirst);
            Result->Cut(k);
            RangeK = Result->GetMaximumDistance();
        }
    }

    if (Node->Second != 0) {

        //test the second element
        dSecond = this->myMetricEvaluator->GetDistance(Node->Second, Element);
        if (Result->GetNumOfEntries() < k) {
            Result->AddPair(Node->Second->Clone(), dSecond);
        }
        else {
            if (dSecond < Result->GetMaximumDistance()) {
                Result->AddPair(Node->Second->Clone(), dSecond);
                Result->Cut(k);
                RangeK = Result->GetMaximumDistance();
            }
        }

        //test the regions
        if (Node->Son != 0) {

            stDistance R = 0.0;

            //find expansion and region
            unsigned uExpansion;
            unsigned uRegion = MAXINT;
            for (uExpansion = 0; uExpansion <= Node->uExpansion; uExpansion++) {
                R += Node->r;
                if (dFirst < R && dSecond < R) { uRegion = 0; break; }
                if (dFirst < R && dSecond >= R) { uRegion = 1; break; }
                if (dFirst >= R && dSecond < R) { uRegion = 2; break; }
            }
            if (uRegion == MAXINT) {
                uExpansion--;
                uRegion = 3;
            }

            //define expansion order
            unsigned uAux = 1;
            int iLeft, iRight;
            vector<unsigned> Expansion;
            Expansion.push_back(uExpansion);
            while (Expansion.size() != Node->uExpansion + 1) {
                iLeft = uExpansion - uAux;
                iRight = uExpansion + uAux;
                if (iLeft >= 0) Expansion.push_back(iLeft);
                if (iRight <= Node->uExpansion) Expansion.push_back(iRight);
                uAux++;
            }
            
            //visit the other regions
            for (unsigned i = 0; i < Expansion.size(); i++) {
                R = Expansion[i] * Node->r;
                if ((dFirst + RangeK >= R) && (dSecond + RangeK >= R)) {
                    R += Node->r;
                    switch (uRegion) {
                        case 0:
                            if ((dFirst < R + RangeK) && (dSecond < R + RangeK)) NearestGuided(Node->Son[Expansion[i] * 3], Element, k, RangeK, false, Result);
                            if (dFirst > dSecond) {
                                if ((dFirst + RangeK >= R) && (dSecond < R + RangeK)) NearestGuided(Node->Son[(Expansion[i] * 3) + 2], Element, k, RangeK, false, Result);
                                if ((dFirst < R + RangeK) && (dSecond + RangeK >= R)) NearestGuided(Node->Son[(Expansion[i] * 3) + 1], Element, k, RangeK, false, Result);
                            }
                            else {
                                if ((dFirst < R + RangeK) && (dSecond + RangeK >= R)) NearestGuided(Node->Son[(Expansion[i] * 3) + 1], Element, k, RangeK, false, Result);
                                if ((dFirst + RangeK >= R) && (dSecond < R + RangeK)) NearestGuided(Node->Son[(Expansion[i] * 3) + 2], Element, k, RangeK, false, Result);
                            }
                            if (Expansion[i] == Node->uExpansion) if ((dFirst + RangeK >= R) && (dSecond + RangeK >= R)) NearestGuided(Node->Son[(Expansion[i] * 3) + 3], Element, k, RangeK, false, Result);
                            break;
                        case 1:
                            if ((dFirst < R + RangeK) && (dSecond + RangeK >= R)) NearestGuided(Node->Son[(Expansion[i] * 3) + 1], Element, k, RangeK, false, Result);
                            if (dFirst - R > R - dSecond) {
                                if ((dFirst < R + RangeK) && (dSecond < R + RangeK)) NearestGuided(Node->Son[Expansion[i] * 3], Element, k, RangeK, false, Result);
                                if (Expansion[i] == Node->uExpansion) if ((dFirst + RangeK >= R) && (dSecond + RangeK >= R)) NearestGuided(Node->Son[(Expansion[i] * 3) + 3], Element, k, RangeK, false, Result);
                            }
                            else {
                                if (Expansion[i] == Node->uExpansion) if ((dFirst + RangeK >= R) && (dSecond + RangeK >= R)) NearestGuided(Node->Son[(Expansion[i] * 3) + 3], Element, k, RangeK, false, Result);
                                if ((dFirst < R + RangeK) && (dSecond < R + RangeK)) NearestGuided(Node->Son[Expansion[i] * 3], Element, k, RangeK, false, Result);
                            }
                            if ((dFirst + RangeK >= R) && (dSecond < R + RangeK)) NearestGuided(Node->Son[(Expansion[i] * 3) + 2], Element, k, RangeK, false, Result);
                            break;
                        case 2:
                            if ((dFirst + RangeK >= R) && (dSecond < R + RangeK)) NearestGuided(Node->Son[(Expansion[i] * 3) + 2], Element, k, RangeK, false, Result);
                            if (dFirst - R > R - dSecond) {
                                if (Expansion[i] == Node->uExpansion) if ((dFirst + RangeK >= R) && (dSecond + RangeK >= R)) NearestGuided(Node->Son[(Expansion[i] * 3) + 3], Element, k, RangeK, false, Result);
                                if ((dFirst < R + RangeK) && (dSecond < R + RangeK)) NearestGuided(Node->Son[Expansion[i] * 3], Element, k, RangeK, false, Result);
                            }
                            else {
                                if ((dFirst < R + RangeK) && (dSecond < R + RangeK)) NearestGuided(Node->Son[Expansion[i] * 3], Element, k, RangeK, false, Result);
                                if (Expansion[i] == Node->uExpansion) if ((dFirst + RangeK >= R) && (dSecond + RangeK >= R)) NearestGuided(Node->Son[(Expansion[i] * 3) + 3], Element, k, RangeK, false, Result);
                            }
                            if ((dFirst < R + RangeK) && (dSecond + RangeK >= R)) NearestGuided(Node->Son[(Expansion[i] * 3) + 1], Element, k, RangeK, false, Result);
                            break;
                        case 3:
                            if ((dFirst + RangeK >= R) && (dSecond + RangeK >= R)) NearestGuided(Node->Son[(Expansion[i] * 3) + 3], Element, k, RangeK, false, Result);
                            if (dFirst > dSecond) {
                                if ((dFirst + RangeK >= R) && (dSecond < R + RangeK)) NearestGuided(Node->Son[(Expansion[i] * 3) + 2], Element, k, RangeK, false, Result);
                                if ((dFirst < R + RangeK) && (dSecond < R + RangeK)) NearestGuided(Node->Son[Expansion[i] * 3], Element, k, RangeK, false, Result);
                                if ((dFirst < R + RangeK) && (dSecond + RangeK >= R)) NearestGuided(Node->Son[(Expansion[i] * 3) + 1], Element, k, RangeK, false, Result);
                            }
                            else {
                                if ((dFirst + RangeK >= R) && (dSecond < R + RangeK)) NearestGuided(Node->Son[(Expansion[i] * 3) + 1], Element, k, RangeK, false, Result);
                                if ((dFirst < R + RangeK) && (dSecond < R + RangeK)) NearestGuided(Node->Son[Expansion[i] * 3], Element, k, RangeK, false, Result);
                                if ((dFirst < R + RangeK) && (dSecond + RangeK >= R)) NearestGuided(Node->Son[(Expansion[i] * 3) + 2], Element, k, RangeK, false, Result);
                            }
                            break;
                    }
                }
            }

            //clear Expansion
            Expansion.clear();

        }

    }

}

template <class ObjectType, class EvaluatorType>
void stFMMTree<ObjectType, EvaluatorType>::GetTreeInformation(void) {
    uNumberOfNodes = 0;
    uTreeSize = 0;
    GetTreeInformation(Root);
}

template <class ObjectType, class EvaluatorType>
void stFMMTree<ObjectType, EvaluatorType>::GetTreeInformation(tFMMNode*& Node) {
    uNumberOfNodes++;
    uTreeSize += 8 + 8 + 4 + (Node->uSon * 4);
    if (Node->Son != 0) {
        for (unsigned i = 0; i < Node->uSon; i++) {
            if (Node->Son[i] != 0) {
                GetTreeInformation(Node->Son[i]);
            }
        }
    }
}

template <class ObjectType, class EvaluatorType>
void stFMMTree<ObjectType, EvaluatorType>::GetTreeHeight(void) {
    uNumberOfLeafNodes = 0;
    dHeightAvg = 0.0;
    uHeightMin = MAXINT;
    uHeightMax = 0;
    GetTreeHeight(Root, 1);
    dHeightAvg /= (double) uNumberOfLeafNodes;
}

template <class ObjectType, class EvaluatorType>
void stFMMTree<ObjectType, EvaluatorType>::GetTreeHeight(tFMMNode*& Node, const unsigned& cuHeight) {
    if (Node->Son != 0) {
        for (unsigned i = 0; i < Node->uSon; i++) {
            if (Node->Son[i] != 0) {
                GetTreeHeight(Node->Son[i], cuHeight + 1);
            }
        }
    }
    else {
        uNumberOfLeafNodes++;
        dHeightAvg += cuHeight;
        if (cuHeight < uHeightMin) uHeightMin = cuHeight;
        if (cuHeight > uHeightMax) uHeightMax = cuHeight;
    }
}


