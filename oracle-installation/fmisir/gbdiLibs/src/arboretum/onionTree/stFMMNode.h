/**********************************************************************
* GBDI Arboretum - Copyright (c) 2002-2004 GBDI-ICMC-USP
*
*                           Homepage: http://gbdi.icmc.usp.br/arboretum
**********************************************************************/
/* ====================================================================
 * The GBDI-ICMC-USP Software License Version 1.0
 *
 * Copyright (c) 2004 Grupo de Bases de Dados e Imagens, Instituto de
 * Ci�ncias Matem�ticas e de Computa��o, University of S�o Paulo -
 * Brazil (the Databases and Image Group - Intitute of Matematical and 
 * Computer Sciences).  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:
 *       "This product includes software developed by Grupo de Bases
 *        de Dados e Imagens, Instituto de Ci�ncias Matem�ticas e de
 *        Computa��o, University of S�o Paulo - Brazil (the Databases 
 *        and Image Group - Intitute of Matematical and Computer 
 *        Sciences)"
 *
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names of the research group, institute, university, authors
 *    and collaborators must not be used to endorse or promote products
 *    derived from this software without prior written permission.
 *
 * 5. The names of products derived from this software may not contain
 *    the name of research group, institute or university, without prior
 *    written permission of the authors of this software.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OF THIS SOFTWARE OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * ====================================================================
 *                                            http://gbdi.icmc.usp.br/
 */

/**
* @file
*
* This file defines the FMM-tree node.
*
* @version 1.0
* $Date: 2008/05/19$
* @author: Ives Ren� Venturini Pola (ives@icmc.usp.br) $
* @author: Caio C�sar Mori Car�lo (ccarelo@icmc.usp.br) $
*/
// Copyright (c) 2008 GBDI-ICMC-USP

#ifndef __STFMMNODE_H
#define __STFMMNODE_H

#include <gbdi/datatype/stTypes.h>
#include <gbdi/datatype/stObject.h>
#include <gbdi/util/stException.h>

/**
* This class implements the node of the FMM-tree. The FMM-tree is built by
* partitioning of the space into X disjoint regions, using two objects as
* pivots and based on the distance between them it partitions the space using.
* multiple expansions.
* @version 1.0
* @author: Ives Ren� Venturini Pola (ives@icmc.usp.br) $
* @author: Caio C�sar Mori Car�lo (ccarelo@icmc.usp.br) $
* @todo Documentation review.
* @ingroup MM
*/

//-----------------------------------------------------------------------------
// Class stFMMNode
//-----------------------------------------------------------------------------
/**
*
* +----------------------------------------------------------------------------------------------------------------+
* | First | Second | Distance |  Son | Radius | SizeOfExpansion| NumberOfExpansions | 
* +----------------------------------------------------------------------------------------------------------------+
*
* <p>The structure of FMMtree node follows:
* @image html fmmnode.png "FMMtree node structure"
*
*/
template <class ObjectType>
class stFMMNode{

   public:

      typedef ObjectType tObject;

      stFMMNode();
      ~stFMMNode();

      tObject* First;
      tObject* Second;
      stFMMNode** Son;

      stDistance r;
      unsigned uExpansion;
      unsigned uSon;

};

template <class ObjectType>
stFMMNode<ObjectType>::stFMMNode(){
   First = 0;
   Second = 0;
   Son = 0;
   r = 0.0;
   uExpansion = 0;
   uSon = 0;
}

template <class ObjectType>
stFMMNode<ObjectType>::~stFMMNode(){
   if (First != 0) {
      delete First;
      First = 0;
   }
   if (Second != 0) {
      delete Second;
      Second = 0;
   }
   if (Son != 0) {
      for (unsigned i = 0; i < uSon; i++) {
         if (Son[i] != 0) {
            delete Son[i];
            Son[i] = 0;
         }
      }
      delete[] Son;
      Son = 0;
   }
}

#endif //__STFMMNODE_H
