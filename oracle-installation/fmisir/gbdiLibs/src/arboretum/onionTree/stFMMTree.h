/**********************************************************************
* GBDI Arboretum - Copyright (c) 2002-2004 GBDI-ICMC-USP
*
*                           Homepage: http://gbdi.icmc.usp.br/arboretum
**********************************************************************/
/* ====================================================================
 * The GBDI-ICMC-USP Software License Version 1.0
 *
 * Copyright (c) 2004 Grupo de Bases de Dados e Imagens, Instituto de
 * Ci�ncias Matem�ticas e de Computa��o, University of S�o Paulo -
 * Brazil (the Databases and Image Group - Intitute of Matematical and 
 * Computer Sciences).  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:
 *       "This product includes software developed by Grupo de Bases
 *        de Dados e Imagens, Instituto de Ci�ncias Matem�ticas e de
 *        Computa��o, University of S�o Paulo - Brazil (the Databases
 *        and Image Group - Intitute of Matematical and Computer 
 *        Sciences)"
 *
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names of the research group, institute, university, authors
 *    and collaborators must not be used to endorse or promote products
 *    derived from this software without prior written permission.
 *
 * 5. The names of products derived from this software may not contain
 *    the name of research group, institute or university, without prior
 *    written permission of the authors of this software.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OF THIS SOFTWARE OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * ====================================================================
 *                                            http://gbdi.icmc.usp.br/
 */

/**
* @file
*
* This file defines the FMM-tree header.
*
* @version 1.0
* $Date: 2008/05/19$
* @author: Ives Ren� Venturini Pola (ives@icmc.usp.br) $
* @author: Caio C�sar Mori Car�lo (ccarelo@icmc.usp.br) $
*/
// Copyright (c) 2008 GBDI-ICMC-USP

#ifndef __stFMMtree__H
#define __stFMMtree__H

//#include <arboretum/stCommon.h>
//#include <arboretum/stTypes.h>
//#include <arboretum/stMetricAccessMethod.h>

#include <gbdi/util/stCommon.h>
#include <gbdi/datatype/stTypes.h>
#include <gbdi/arboretum/stMetricAccessMethod.h>
#include "stFMMNode.h"

template <class ObjectType, class EvaluatorType>
class stFMMTree: public stMetricAccessMethod<ObjectType, EvaluatorType>{
    
public:

    typedef ObjectType tObject;
    typedef stResult<ObjectType> tResult;
    typedef stFMMNode<ObjectType> tFMMNode;

    stFMMTree(const unsigned& cuReplacementType);
    stFMMTree(const unsigned& cuReplacementType, const unsigned& cuFixedNumberOfExpansions);
    ~stFMMTree();

    bool Add(tObject* Element);
    tResult* RangeQuery(tObject* sample, stDistance range);
    tResult* NearestQuery(tObject* sample, int k, bool tie = false);

    //replacement
    enum ReplacementType {rtNOREPLACEMENT, rtKEEPSMALL, rtMIN, rtLIKEDAD, rtRANDOM, rtMAX};

    //size
    void GetTreeInformation(void);
    unsigned uNumberOfNodes;
    unsigned uTreeSize;

    void GetTreeHeight(void);
    unsigned uNumberOfLeafNodes;
    double dHeightAvg;
    unsigned uHeightMin;
    unsigned uHeightMax;

private:

    //attributes
    unsigned uTypeOfExpansion;
    unsigned uFixedNumberOfExpansions;
    tFMMNode* Root;

    //replacement
    unsigned uReplacementType;

    void CreateSubspaces(tFMMNode*& Father, tFMMNode*& Node);
    unsigned ChooseSubspace(tFMMNode*& Node, const stDistance& cdFirst, const stDistance& cdSecond);
    void Replacement(tFMMNode*& Father, tFMMNode*& Node, tObject*& Element, stDistance& dFirst, stDistance& dSecond);
    
    void Insert(tFMMNode*& Father, tFMMNode*& Node, tObject* Element);
    void Range(tFMMNode*& Node, tObject* Element, const stDistance& r, tResult* Result);
    void NearestGuided(tFMMNode*& Node, tObject* Element, const unsigned& k, stDistance& RangeK, const bool& bTie, tResult* Result);

    void GetTreeInformation(tFMMNode*& Node);
    void GetTreeHeight(tFMMNode*& Node, const unsigned& cuHeight);

}; //end stFMMTree

//#include "stFMMTree.cc"
#endif //end __stFMMtree__H
