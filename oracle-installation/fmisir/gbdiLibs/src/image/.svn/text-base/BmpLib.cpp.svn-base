#include <gbdi/image/BmpLib.hpp>

/**
* Empty constructor of BMPImage Class.
* It's advised when you want to create
* a new BMP File.
* NOTE: For default all BMP Files are opened
* in colored mode with 8 bits per pixel.
*/
BMPImage::BMPImage(){
//    initialize(); // is it necessary?
}

/**
* Constructor of BMPImage Class.
* It's advised when you want to open
* a BMP File.
* NOTE: For default all BMP Files are opened
* in colored mode with 8 bits per pixel.
*
* @param filename: The name of the file.
*/
BMPImage::BMPImage(string filename) throw (artemis::FileException*, artemis::FullHeapException*) {
    initialize();
    openImage(filename);
}

BMPImage::BMPImage(unsigned char * buffer, unsigned int length) throw (std::exception) {
    throw logic_error("Unimplemented method BMPImage::openImage(unsigned char * buffer, unsigned int length)");
}

/**
* Destructor of BMPImage Class.
*/
BMPImage::~BMPImage(){
    deletePixelMatrix();
}

/**
* Open the BMP images.
* If file exists, the image are loaded in the pixel matrix.
*
* @param filename: Name of the Image to open.
* @exception FileException Throwed when the file does not exists.
* @exception FullHeapException Insufficient space for memory allocation.
* @see Pixel.
*/
void BMPImage::openImage(string filename) throw (artemis::FileException*, artemis::FullHeapException*){

    //A OpenCv type to recovery a Pixel
    CvScalar s;
    //A OpenCv structure to load a image file
    IplImage * image;

    setFilename(filename);
    image = cvLoadImage(this->getFilename().c_str(), 3);

    if (!image)
       throw new artemis::FileException(0, "The image file cannot be opened or the file does not exists", filename);

    setImageID(0);

    //JPG is a 3 channel colored image
    setChannels(image->nChannels);

    //Properties of the Object
    //Size is the pixel area
    //Here is the calculus
    setSize((image->height)*(image->width));
    setWidth(image->width);
    setHeight(image->height);
    //Transfer RGB from IplImage to Matrix of Pixels
    //The Matrix of Pixels can be accessed now
    //Full Load of the jpg image
    //This function load the JPEG image by OpenCV

    //Try allocate pixel matrix in the memory
    createPixelMatrix(getWidth(), getHeight());

    //Converts the IpLImage OpenCVStructure to Pixel Matrix
    for(int x = 0; x < getWidth(); x++){
      for(int y = 0; y < getHeight(); y++){
          //Catch the RGB from opened Image and tranfer to the matrix of pixels
          s = cvGet2D(image, y, x);
          Pixel *aux = new Pixel((const char)s.val[2], (const char)s.val[1], (const char)s.val[0]);
          setPixel(x, y, aux);
          delete(aux);
      }
    }

    //Destroy the OpenCv structure
    cvReleaseImage(&image);

    setBitsPerPixel(8); // WARNING: Is it always 8?
}


/**
* Clones a current Image.
* If, for any reason the Image cannot be copied,
* then an exception is thrown
*/
Image * BMPImage::clone() throw (std::exception) {
    //Create a new instance of BMP Image and copy
    //the features of the current instance to the new instance.
    Image * i = new BMPImage();
    copyData(i);
    return i;
}

