import json

def main():
    try:
        path = "/configs/default/configmap/configmap.json"
        f = open(path,"r")
        config = f.read()
        f.close()
        return "success"
    except Exception as e:
        return str(e)
