from sys import argv
import json
import cx_Oracle

line = '{"connection" : {"username" : "dbuser", "password": "dbpassword", "host": "172.16.16.105", "sid": "orcl"}}'


def main():
	try:
		input_json = json.loads(line)
		connection = input_json['connection']
		sql = 'INSERT INTO MYTABLE(MYID) VALUES (1)'
		con = cx_Oracle.connect(connection['username']+'/'+connection['password']+'@'+connection['host']+'/'+connection['sid'], 
				encoding="UTF-8", nencoding="UTF-8")
		cur = con.cursor()
		cur.execute(sql)
		cur.close()
		con.commit()
		return 'successfully inserted on db'
	except Exception as e:
		return str(e)