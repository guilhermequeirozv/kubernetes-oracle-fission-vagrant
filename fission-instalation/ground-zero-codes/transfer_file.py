import paramiko
import json

line = '{"remote_info_src":{"server":"172.16.16.101","port":"22","user":"vagrant","password":"vagrant"},"remote_info_dest":{"server":"172.16.16.105","port":"22","user":"vagrant","password":"vagrant"},"file_info":{"directory":"/home/vagrant","name":"test.txt","destiny":"/home/vagrant"}}'

def main():
	try:
		json_input = json.loads(line)
		remote_info_dest = json_input['remote_info_dest']
		remote_info_src = json_input['remote_info_src']
		file_info = json_input['file_info']

		ssh = paramiko.SSHClient()
		ssh.load_system_host_keys()
		ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		ssh.connect(remote_info_dest['server'], remote_info_dest['port'], remote_info_dest['user'], remote_info_dest['password'])

		ssh_src = paramiko.SSHClient()
		ssh_src.load_system_host_keys()
		ssh_src.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		ssh_src.connect(remote_info_src['server'], remote_info_src['port'], remote_info_src['user'], remote_info_src['password'])

		sftp_src = ssh_src.open_sftp()
		myfile = sftp_src.get(file_info['directory']+'/'+file_info['name'],file_info['name'])

		sftp = ssh.open_sftp()
		sftp.put(file_info['name'],file_info['destiny']+'/'+file_info['name'])

		sftp.close()
		sftp_src.close()
		return "success"
	except Exception as e:
		return str(e)
