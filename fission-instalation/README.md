### 1) Create storage class.

```
kubectl apply -f sc.yaml
```

### 2) In pv.yaml define path and values (node name) variable. Then create persistent volume.

```
kubectl apply -f pv.yaml
```

### 3) Install Helm and create tiller account.

> [Installation of Helm](https://docs.fission.io/docs/installation/#helm)


### 4) Install Fission with values.yaml.

```
export FISSION_NAMESPACE="fission"
kubectl create namespace $FISSION_NAMESPACE
helm install --namespace $FISSION_NAMESPACE --name-template fission \
    --set serviceType=NodePort,routerServiceType=NodePort,logger.enableSecurityContext=true,prometheus.enabled=false \
    -f values.yaml \
    https://github.com/fission/fission/releases/download/1.12.0/fission-all-1.12.0.tgz
```

### 5) Log into your DockerHub from command line.

```
docker login --username=yourhubusername --password=yourpassword
```

### 6) Build docker image containing cx_oracle and paramiko. USER is your DockerHub username.

```
docker build -t USER/python-env .
docker push USER/python-env
```

### 7) Create environment.

```
fission env create --name python --image USER/python-env
```

### 8) Forwarding ports.

```
kubectl --namespace fission port-forward --address 172.16.16.101 $(kubectl --namespace fission get pod -l svc=router -o name) 30003:8888 &
```